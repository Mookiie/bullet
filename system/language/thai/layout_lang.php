<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  // GLOBAL
  $lang['wait'] = 'กรุณารอสักครู่';
  $lang['signout'] = 'ออกจากระบบ';
  $lang['account'] = 'บัญชีผู้ใช้';
  $lang['send'] = 'ส่งคำขอ';
  $lang['cancel'] = "ยกเลิก";
  $lang['list'] = "รายการ";
  $lang['load'] = "กำลังโหลดข้อมูล";
  $lang['dismiss'] = "ปฏิเสธ";
  $lang['assign'] = "มอบหมายงาน";
  $lang['pass'] = "ผ่าน";
  $lang['fail'] = "ไม่ผ่าน";
  $lang['edit'] = "แก้ไข";
  $lang['close'] = "ปิด";
  $lang['detail'] = "เพิ่มรายละเอียดงาน";
  $lang['accept'] = "รับงาน";

  // Nav
  $lang['nav_homepage'] = 'หน้าหลัก';
  $lang['nav_requestmepage'] = 'คำขอของฉัน';
  $lang['nav_workmepage'] = 'งานของฉัน';
  $lang['nav_requestpage'] = 'เพิ่มขอใช้บริการ';
  $lang['nav_approvepage'] = 'รายการรออนุมัติ';
  $lang['nav_ticketpage'] = 'มอบหมายงานตามคำขอ';
  $lang['nav_reportpage'] = 'รายงานการให้บริการ';
  $lang['nav_administratorpage'] = 'จัดการผู้ใช้ระบบ';
  $lang['nav_systempage'] = 'ระบบ';
  $lang['nav_reportpage'] = 'รายงาน';
  $lang['nav_reportsystempage'] = 'รายงานระบบ';

  // Dashboard
  $lang['dashboard'] = 'คำขอใช้บริการของฉัน';
  $lang['request'] = 'คำขอใช้บริการ';
  $lang['wait'] = 'รอให้บริการ';
  $lang['working'] = 'กำลังดำเนินงาน';
  $lang['check'] = 'รอตรวจสอบ';
  $lang['success'] = 'คำขอสำเร็จ';
  $lang['add'] = 'เพิ่มคำขอ';

  // working
  $lang['working'] = 'งานของฉัน';

  //request
  $lang['request'] = 'คำขอใช้บริการ';
  $lang['select_please'] = 'กรุณาเลือก';
  $lang['input_please'] = 'กรุณากรอก';
  $lang['request_company'] = 'บริษัทที่ต้องการขอใช้บริการ';
  $lang['request_branch'] = 'สาขาที่ต้องการขอใช้บริการ';
  $lang['request_dep'] = 'แผนกที่ต้องการขอใช้บริการ';
  $lang['request_site'] = 'ไชต์งานที่ต้องการขอใช้บริการ';
  $lang['request_team'] = 'ทีมที่ต้องการขอใช้บริการ';
  $lang['request_title'] = 'หัวข้อคำขอใช้บริการ';
  $lang['request_detail'] = 'รายละเอียดคำขอใช้บริการ';
  $lang['request_file'] = 'แนบรายละเอียดคำขอใช้บริการ';

  // ticket
  $lang['ticket'] = 'คำขอให้บริการของหน่วยงาน';
  $lang["ticket_manage"] = "จัดการคำขอ";
  $lang['ticket_status'] = 'สถานะ';
  $lang['ticket_title'] = 'หัวข้อ';
  $lang['ticket_date'] = 'วันที่ขอใช้';
  $lang['ticket_progress'] = 'ความคืบหน้า';
  $lang["table_label_show"] = "แสดง";
  $lang["table_label_row"] = "แถว";
  $lang["table_search"] = "ค้นหาชื่อหัวข้อ";
  $lang["assign_type"] = "ประเภทการให้บริการ";
  $lang["assign_team"] = "รายชื่อทีมให้บริการ";
  $lang["assign_user"] = "รายชื่อเจ้าหน้าที่ให้บริการ";
  $lang["assign_finish"] = "วันที่เสร็จสิ้น";
  $lang["type_user"] = "บุคคล";
  $lang["type_team"] = "ทีม";
  $lang["tracking"] = "รายการติดตาม";
  $lang['working_detail'] = 'รายละเอียดการให้บริการ';
  $lang['working_file'] = 'แนบรายละเอียดการให้บริการ';


  // Report
  $lang['report'] = 'รายงาน';
  $lang['reportSystem'] = 'รายงานการใช้งานระบบ';

  // administrator
  $lang['administrator'] = 'จัดการผู้ใช้ระบบ';
  $lang['invite'] = 'เชิญผู้ใช้งาน';
  $lang['view'] = 'ข้อมูลผู้ใช้';


  // status
  $lang['status_wait'] = 'รอจัดงาน';
  $lang['status_assign'] = 'รอดำเนินการ';
  $lang['status_working'] = 'กำลังดำเนินการ';
  $lang['status_check'] = 'รอตรวจสอบ';
  $lang['status_success'] = 'สำเร็จ';
  $lang['status_dismiss'] = 'ยกเลิก';
  $lang['status_all'] = 'ทั้งหมด';

  // systems
  $lang['createcompany'] = 'สร้างบริษัท';
  $lang['createuser'] = 'สร้างผู้ใช้';



?>
