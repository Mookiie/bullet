<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  // login

  $lang['signin'] = 'ลงชื่อเข้าระบบ';
  $lang['username'] = 'ชื่อผู้ใช้';
  $lang['username_request'] = 'กรุณากรอกชื่อผู้ใช้';
  $lang['password'] = 'รหัสผ่าน';
  $lang['password_request'] = 'กรุณากรอกรหัสผ่าน';
  $lang['signup'] = 'ลงทะเบียน';
  $lang['noaccount'] = 'คุณยังไม่มีชื่อผู้ใช้?';//Don't have an account?
  $lang['forget'] = 'ลืมรหัสผ่าน?';
  $lang['login'] = 'เข้าสู่ระบบ';
  $lang['remember'] = 'จดจำฉัน';



?>
