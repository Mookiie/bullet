var company_id = "";
$(document).ready(function(){
  closeloader();
  CountTicketStatus("count-assign",company_id,"(1)");
  CountTicketStatus("count-working",company_id,"(2,4)");
  CountTicketStatus("count-check",company_id,"(5)");
  CountTicketStatus("count-success",company_id,"(3,6,7)");
  CountTicketStatus("count-record",company_id,"");
  company_id = $(this).val();
  ticketservice(company_id,"");
})

$("#company").change(function() {
  company_id = $(this).val();
  CountTicketStatus("count-assign",company_id,"(1)");
  CountTicketStatus("count-working",company_id,"(2,4)");
  CountTicketStatus("count-check",company_id,"(5)");
  CountTicketStatus("count-success",company_id,"(3,6,7)");
  CountTicketStatus("count-record",company_id,"");
  ticketservice(company_id,"");
})

function CountTicketStatus(id,company,status) {
  var api_url = getApi('ticket/countservicestatus');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"company_id":company, "status":status}
  })
  .done(function(data) {
      var obj = JSON.parse(data);
      $("#"+id).html(obj);
  })
}

function nextpage(el){
  console.log("nextpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var next = $("#nowpage")[0].innerHTML;
      next = parseFloat(next);
      next ++;
  var start = (next-1) * has;
  if (next <= parseFloat($(".totalpage")[0].innerHTML)) {
             $("#nowpage").html(next);
             $(".file-container").html("")
             $(".file-container").getTables({"data" : service_me ,"page": start , "view" : $("#row_show").val() });
   }
}

function prevpage(el) {
  console.log("prevpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var prev = $("#nowpage")[0].innerHTML;
      prev = parseFloat(prev);
      prev --;
  var start = (prev-1) * has;
  if (prev >  0) {
              $("#nowpage").html(prev);
              $(".file-container").html("")
              $(".file-container").getTables({"data" : service_me ,"page": start , "view" : $("#row_show").val() });
  }
}

$.fn.getTables = function (obj){
    var view = obj["view"];
        view = parseFloat(view);
    var data = obj["data"];
    var page = obj["page"];
    var row = data.length;
    for (var i = 0; i < data.length; i++) {
         var display = "";
         if (i <  (page)) {
             display = "none";
         }
         if (i > (view - 1)+page) {
             display = "none";
         }
         var row = genRowTicket(i,data[i],display);
         $(this).append(row);
    }
    var has = 0;
        var tr = $(".file-container tr");
        tr.each(function(index, el) {
            if($(el).css('display') != "none"){
               has++;
            }
        });
    var total = Math.ceil((tr.length)/view);
    $(".totalpage").html(total);

    $('.tr-ticket').click(function() {
      console.log($(this).data('id'));
      var ticket_id =$(this).data('id');
      var api_url = getApi('ticket/profile');
      var token = localStorage.getItem('token');
      var obj_ticket;
      $.ajax({
        url: api_url,
        type: 'GET',
        headers:{"token":token},
        data :{"ticket_id":ticket_id},
        beforeSend:function(){
          preloader();
        }
        })
        .done(function(data) {
          obj_ticket = JSON.parse(data);
            console.log(obj_ticket);
            if (obj_ticket.status==200) {
              var connectURL = getUrl("ticket/ticketDialog");
              var formData = {"ajax":true};
                  formData["data"] = obj_ticket['data'] ;
              $.ajax({
                      url: connectURL,
                      type: 'POST',
                      data: formData
                    }).done(function(data) {
                      closeloader()
                      var obj_modal = JSON.parse(data);
                      $(".modal-area").append(obj_modal["modal"]);
                      $("#TicketDialog").bulletModal({"backdrop":true,"static":true});
                      $(".tabs-member").tabs();
                      $("#work_id").val(formData.data.work_id);
                      // profile-ticket
                      $("#assign").click(function() {
                        $("a#assign_config").trigger("click")
                      })
                      $("#dismiss-t").click(function () {
                        var api_url = getApi('ticket/dismissassign');
                        var token = localStorage.getItem('token');
                        $.ajax({
                                url: api_url,
                                type: 'GET',
                                headers:{"token":token},
                                data: {"ticket_id":ticket_id},
                                beforeSend:function(){
                                  preloader();
                                }
                              }).done(function(data) {
                                closeloader()
                                var obj = JSON.parse(data);
                                if (obj.status == 200) {
                                  swal({
                                         title: 'Success!',
                                         text: "",
                                         type: 'success',
                                         showCancelButton: false,
                                         confirmButtonColor: '#3085d6',
                                         confirmButtonText: 'OK!'
                                      }).then((result) => {
                                         if (result.value) {
                                          location.reload();
                                         }
                                     })
                                }
                                else {
                                  Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                                }
                              })
                      })
                      // ticket_attached
                      if (formData.data.ticket_image != '') {
                        $(".attached").css("display","");
                        var pathtxt = $("#pathtxt").val();
                         if(pathtxt != ""){
                           PDFObject.embed(pathtxt, "#framPDF");
                         }
                        $("#download").click(function () {
                          console.log("download");
                          var api_url = getApi('ticket/download');
                          var token = localStorage.getItem('token');
                          $.ajax({
                                  url: api_url,
                                  type: 'POST',
                                  headers:{"token":token},
                                  data: {"ticket_id":ticket_id}
                                }).done(function(data) {

                                })
                        })
                      }
                      // tracking
                      if (formData.data.ticket_status == 4||formData.data.ticket_status == 5||formData.data.ticket_status == 7) {
                        $(".detail").css("display","");
                        $(".detail").click(function () {
                          console.log("detail");
                          getDetailWork(ticket_id);
                        })
                      }
                      $("#tracking").click(function () {
                        var api_url = getApi('ticket/tracking');
                        var token = localStorage.getItem('token');
                        $.ajax({
                          url: api_url,
                          type: 'GET',
                          headers:{"token":token},
                          data :{"ticket_id":ticket_id},
                          beforeSend:function(){
                            preloader();
                          }
                          })
                          .done(function(data) {
                            closeloader()
                            var obj = JSON.parse(data);
                            if (obj.status == 200) {
                              var tracking_table = '';
                                for (var i = 0; i < obj.data.length; i++) {
                                  var tracking = obj.data[i]
                                  tracking_table += getTrackingTicket(tracking);
                                  $("#tracking_list").html(tracking_table);
                                }
                            }else {
                              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                            }

                          })
                      })
                      // set-assign
                      var date = new Date();
                      var dd = date.getDate();
                      var mm = date.getMonth();
                      var yy = date.getFullYear();
                      $('.datepicker').pickadate({
                        min: new Date(yy,mm,dd),
                        selectMonths: true,
                        selectYears: 5,
                        today: 'Today',
                        clear:'',
                        cancel:'Cencel',
                        defaultDate:new Date(yy,mm,dd),
                        closeOnSelect: true ,
                        setDefaultDate:true,
                        format : "yyyy-mm-dd",
                      })
                        if (formData.data.ticket_status == 1 || formData.data.ticket_status == 2 || formData.data.ticket_status == 4) {
                        if (profile_me.data.level.level_assign == 1) {
                          if (formData.data.ticket_status == 1 ) {
                            $(".assign-menu").css("display","");
                          }
                          $(".assign-config").css("display","")
                        }// profile_me.data.level.level_assign == 1
                        if (obj_ticket.data.ticket_site == "") {
                          $(".select-team").attr("disabled",'disabled');
                        }
                        $("#type").change(function() {
                          if ($(this).val()==1) {
                            $(".assign-user").css("display","")
                            $(".assign-team").css("display","none")
                            $(".assign_user").removeAttr("disabled")
                            getuserservice(obj_ticket['data'],'')
                          }//person
                          else if ($(this).val()==2) {
                            $(".assign-team").css("display","")
                            $(".assign-user").css("display","none")
                            $(".assign_team").removeAttr("disabled")
                            $(".assign_user").attr("disabled", 'disabled');
                            getteambysite(obj_ticket['data']['ticket_site'])
                          }//team
                        })//$("#type").change
                        // $("#assign_team").change(function () {
                        //   $(".assign-user").css("display","")
                        //   $(".assign_user").removeAttr("disabled",'');
                        //   getuserservice(obj_ticket['data'],$(this).val())
                        // })//$(".assign_team").change
                        $("form#formAssign").submit(function(ev){
                            ev.preventDefault();
                            if ($("#assign_finish").val()=='') {
                              $("#assign_finish").trigger("click");
                            }else {
                              var api_url = getApi('ticket/assign_working');
                              var token = localStorage.getItem('token');
                              var work_assign_to;
                              if ($("#type").val() == 1) {
                                work_assign_to = $("#assign_user").val();
                              }else {
                                work_assign_to = $("#assign_team").val();
                              }
                              $.ajax({
                                url: api_url,
                                type: 'POST',
                                headers:{"token":token},
                                data :{"work_id":$("#work_id").val(),
                                       "ticket_id":ticket_id,
                                       "work_type":$("#type").val(),
                                       "work_assign_to":work_assign_to,
                                       "work_finish":$("#assign_finish").val()},
                                beforeSend:function(){
                                  preloader();
                                }
                                })
                                .done(function(data) {
                                  closeloader();
                                  var obj = JSON.parse(data);
                                    if (obj.status==200) {
                                      swal({
                                             title: 'Success!',
                                             text: "",
                                             type: 'success',
                                             showCancelButton: false,
                                             confirmButtonColor: '#3085d6',
                                             confirmButtonText: 'OK!'
                                          }).then((result) => {
                                             if (result.value) {
                                              location.reload();
                                             }
                                         })
                                    }else {
                                      Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                                    }
                                })


                            }
                        })
                      }//ticket_status == 1, 2, 4
                      if (formData.data.ticket_status > 1) {
                        $(".dialogservice").css("display","");
                      }
                  })// load modal
            }else {
              closeloader()
              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
            } //obj.status==200
        }) // load data ticket/detail
    })//$('.tr-ticket').click
  }

function getviewTables(ele){
  var view = ele.val();
  $(".file-container").html("")
  $(".file-container").getTables({"data" : service_me ,"page": 0 , "view" : $("#row_show").val() });
  $("#nowpage").html(1);
}

function searchInTables(ele){
  var filter = ele.val().toUpperCase();
  var filter_obj = ticket_me;
  var tr = $(".file-container tr");
  for (var r = 0; r < tr.length; r++) {
    var row = tr[r].dataset.row;
    var td = $("#tr_file_"+row+" td.sort");
    var str = "";
      for (var i = 0; i < td.length; i++) {
        str += td[i].innerHTML;
      }
      if(str.toUpperCase().indexOf(filter) > -1){
        $("#tr_file_"+row).css({"display":""});
      }else{
        $("#tr_file_"+row).css({"display":"none"});
      }
  }
}

function ticket_wait() {
  ticketservice(company_id,"(1)")
}
function ticket_working() {
  ticketservice(company_id,"(2,4)")
}
function ticket_check() {
  ticketservice(company_id,"(5)")
}
function ticket_success() {
  ticketservice(company_id,"(3,6,7)")
}
function getuserservice(obj_ticket,team) {
  var api_url = getApi('user/getuserservice');
  var token = localStorage.getItem('token');
  var formdata_user = {"company_id":obj_ticket['ticket_company'],
                       "branch_id":obj_ticket['ticket_branch'],
                       "dep_id":obj_ticket['ticket_dep'],
                       "site_id":obj_ticket['ticket_site'],
                       "team_id":team};
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :formdata_user,
    beforeSend:function(){
      preloader();
    }
    })
    .done(function(data) {
      closeloader();
      var obj_user = JSON.parse(data);
      if (obj_user.status == 200) {
        $(".assign_user").html("<option value='' disabled selected>"+lang.request.user+"</option>");
        for (var i = 0; i < obj_user.data.length; i++) {
          $(".assign_user").append("<option value="+obj_user.data[i]['uid']+">"+obj_user.data[i]['fname']+" "+obj_user.data[i]['lname']+"</option>");
        }
      }else if (obj_user.status == 404) {
          $(".assign_user").removeAttr("disabled");
          $(".assign_user").html("<option value=''>"+lang.request.usernotfound+"</option>");
      }
      else {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
      }
    })
}

function getteambysite(site) {
  var api_url = getApi('team/getdatabysite');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"site_id":site},
    beforeSend:function(){
      preloader();
    }
    })
    .done(function(data) {
      closeloader();
      console.log(data);
      var obj_team = JSON.parse(data);
      if (obj_team.status == 200) {
        $(".assign_team").html("<option value='' disabled selected>"+lang.request.team+"</option>");
        for (var i = 0; i < obj_team.data.length; i++) {
          $(".assign_team").append("<option value="+obj_team.data[i]['team_id']+">"+obj_team.data[i]['team_name']+"</option>");
        }
      }else if (obj_team.status == 404) {
          $(".assign_team").removeAttr("disabled");
          $(".assign_team").html("<option value='' disabled selected>"+lang.request.teamnotfound+"</option>");
      }
      else {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
      }
    })

}
