var lang = new Lang();
var profile_me;
// data ms
var data_level;
var data_company;
var data_branch;
var data_department;
var data_site;
var data_team;
// data ticket
var ticket_me;
var work_me;
var service_me;
var ticket_detail;
var detail_work;
// hour()
var Interval

function Lang (){
    var jsonLang = JSON.parse($("#jsonLang").html());
    this.error = jsonLang.error_message;
    this.request = jsonLang.request_message;
}

function getUrl(path){
    var config = 'bullet';
    var origin = window.location.origin;
    return origin+"/"+config+"/"+path
}

function getApi(path){
    var config = 'bullet_api';
    var origin = window.location.origin;
    return origin+"/"+config+"/"+path
}

// function
function Getprofile()
 {
  var profile = getApi('user/profile');
  var token = localStorage.getItem('token');
  $.ajax({
    url: profile,
    type: 'GET',
    headers:{"token":token},
  })
  .done(function(data) {
      profile_me = JSON.parse(data);
      console.log(profile_me.data);
      $(".auth_profile").attr("src",profile_me.data.image);
      $(".name").append(profile_me.data.fname+" "+profile_me.data.lname);
      $(".email").append(profile_me.data.email);
      if (profile_me.data.company_id == "") {
        $(".company-menu").css("display","")
        $(".company-select").css("display","")
        $(".company-card").css("display","")
        Company();
      }
      if (profile_me.data.level.level_assign == 1) {
        $(".ticket").css("display","");
      }
      if (profile_me.data.level.level_qc == 1) {
         $(".ticket_check").css("display","");
      }
      if (profile_me.data.level.level_report == 1) {
        $(".report").css("display","");
      }
      if (profile_me.data.level.level_admin == 1) {
        $(".administrator").css("display","");
        $(".reportsystem").css("display","");
      }
      if (profile_me.data.level.level_config == 1) {
        $(".system").css("display","")
      }
  })
}

function getProfileDetail() {

}
// คำขอของฉัน
function ticketme(status) {
  var api_url = getApi('ticket/me');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"status":status}
  })
  .done(function(data) {
      closeloader();
      var obj = JSON.parse(data);
      if (obj.status == 200) {
        ticket_me = obj.data;
        $("#nowpage").html("1");
        $(".file-container").html("")
        $(".file-container").getTables({"data" : ticket_me ,"page": 0 , "view" : $("#row_show").val() });
      }else if (obj.status == 404) {
        $(".file-container").html("")
      }
  })
}
// งานของฉัน
function ticketWork(company,status) {
  var api_url = getApi('ticket/work');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"company_id":company,"status":status}
  })
  .done(function(data) {
      closeloader();
      var obj = JSON.parse(data);
      if (obj.status == 200) {
        work_me = obj.data;
        $("#nowpage").html("1");
        $(".file-container").html("")
        $(".file-container").getTables({"data" : work_me ,"page": 0 , "view" : $("#row_show").val() });
      }else if (obj.status == 404) {
        $(".file-container").html("")
      }
  })
}
// หน่วยงาน
function ticketservice(company,status) {
  var api_url = getApi('ticket/service');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"company_id":company,"status":status}
  })
  .done(function(data) {
      closeloader();
      var obj = JSON.parse(data);
      if (obj.status == 200) {
        service_me = obj.data;
        $("#nowpage").html("1");
        $(".file-container").html("")
        $(".file-container").getTables({"data" : service_me ,"page": 0 , "view" : $("#row_show").val() });
      }else if (obj.status == 404) {
        $(".file-container").html("")
      }
  })
}

function getDetailWork(ticket) {
  var api_url = getApi('ticket/detail_work');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"ticket_id":ticket}
  }).done(function(data) {
    var obj = JSON.parse(data);
    console.log(obj);
    if (obj.status == 200) {
      $(".show-detail").html("")
        var detail_work = '';
          for (var i = 0; i < obj.data.length; i++) {
            var detail = obj.data[i]
            detail_work += getShowDetail(detail);
            $(".show-detail").html(detail_work);
          }
      }else {
        var detail = "";
        detail += "<div class='center' style='padding:25px'>"
        detail += "<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.notfound
        detail += "</div>"
        $(".show-detail").html(detail)
      }
      if (obj.count > 0) {
        $("#success").removeAttr("disabled")
      }
  })
}

// function getUser(company,status) {
//   var api_url = getApi('user/getuser');
//   var token = localStorage.getItem('token');
//   $.ajax({
//     url: api_url,
//     type: 'GET',
//     headers:{"token":token},
//     data :{"company_id":company,"status":status}
//   })
//   .done(function(data) {
//       closeloader();
//       var obj = JSON.parse(data);
//       if (obj.status == 200) {
//         users = obj.data;
//         $("#nowpage").html("1");
//         $(".file-container").html("")
//         $(".file-container").getTables({"data" : users ,"page": 0 , "view" : $("#row_show").val() });
//       }else if (obj.status == 404) {
//         $(".file-container").html("")
//       }
//   })
// }

//
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isType(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
    case 'pdf':
    case '':
        //etc
        return true;
    }
    return false;
}

// date
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    // if (month.length < 2) month = '0' + month;
    // if (day.length < 2) day = '0' + day;

    // return [year, month, day].join('-');
    console.log(year+"-"+month+"-"+day);
}

// nl2br
function nl2br (str, is_xhtml) {
     var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
     return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

// validate
function addErr(type,msg) {
  $('#'+type).addClass("invalid");
  $('#label'+type).addClass("error");
  $('#label'+type).html(msg);

}
function rmErr(type) {
  $('#'+type).removeClass("invalid");
  $('#label'+type).removeClass("error");
  $('#label'+type).html('');
}

$.fn.validate_err = function (msg){
    var id = $(this)[0].id;
    $("#"+id+"_group").addClass('error');
    $("#"+id+"_feedback").addClass('error').html(msg);
    $(this).blur(function(event) {
      $("#"+id+"_group").removeClass('error');
      $("#"+id+"_feedback").removeClass('error').html("");
    });
}

$.fn.validateWarningBlank = function (){
    var input = $(this);
    for (var i = 0; i < input.length; i++) {
         if (input[i].value == "") {
              var id = input[i].id;
              $("#"+id+"_group").addClass('warning');
              $("#"+id+"_feedback").html("<span class='fa fa-exclamation-triangle'></span> "+lang.error.input_blank).addClass('warning');
              $("#"+id+"").blur(function(event) {
              $("#"+id+"_group").removeClass('warning');
              $("#"+id+"_feedback").html("").removeClass('warning');
              });
              return false;
         }
    }
    return true;
  }

$.fn.validateWarning = function(msg){
    $(this).parent('.select-wrapper').parent('.input-field').addClass('warning');
    $(this).parent('.select-wrapper').parent('.input-field').children('.feedback').html(msg);

    $(this).parent('.input-field').addClass('warning');
    $(this).parent('.input-field').children('.feedback').html(msg);
    $(this).focus();
}

function onremove_validate(el){
   if ($(el).parent('.input-field').hasClass('error')) {
       $(el).parent('.input-field').removeClass('error');
       $(el).parent('.input-field').children('.feedback').html("");
   }
   $(el).parent('.select-wrapper').parent('.input-field').removeClass('error');
   $(el).parent('.select-wrapper').parent('.input-field').children('.feedback').html("");

   if ($(el).parent('.input-field').hasClass('warning')) {
       $(el).parent('.input-field').removeClass('warning');
       $(el).parent('.input-field').children('.feedback').html("");

   }
   $(el).parent('.select-wrapper').parent('.input-field').removeClass('warning');
   $(el).parent('.select-wrapper').parent('.input-field').children('.feedback').html("");
}

// loader
function preloader() {
  $(".loader").show();
  $(".preload-backdrop").css({"display":"block"});
  Interval = setInterval(function(){hour()},2500);
}
function closeloader() {
  $(".loader").hide();
  $(".preload-backdrop").css({"display":"none"})
  clearInterval(Interval);
}
function hour(){
    setTimeout(function(){
          // $(".page-load").removeClass('fa-hourglass-start').addClass('fa-hourglass-half').css({"transform":"rotate(0deg"});
          $(".point-wait").html("..");
    },500);
    setTimeout(function(){
          // $(".page-load").removeClass('fa-hourglass-half').addClass('fa-hourglass-end');
          $(".point-wait").html("...");
    },1000);
    setTimeout(function(){
          // $(".page-load").removeClass('fa-hourglass-end').css({"transform":"rotate(360deg"});
          $(".point-wait").html("");
    },1500);
    setTimeout(function(){
          // $(".page-load").addClass('fa-hourglass-start');
          $(".point-wait").html(".");
          // .addClass('fa-hourglass-start');
    },2000);
  }

// var formData = new FormData(this);
// console.log(formData);
// for (var value of formData.values()) {
//     console.log(value);
// }

// toast
// Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.request_success, 40000);
// Materialize.toast("<span class='warning fas fa-exclamation-circle'></span> &nbsp;"+lang.error.request_warning, 40000);
// Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.request_error, 40000);
