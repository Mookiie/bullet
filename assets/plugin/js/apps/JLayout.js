$(document).ready(function() {
  $("body").layoutFunctions();
});
(function ($){
  $.fn.layoutFunctions = function(){
        $(".btn").addClass('waves-effect');
        $(".nav-top-dropdown").dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: false, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: true, // Displays dropdown below the button
            alignment: 'right', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
          });
          $('.dropdown-button').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false, // Does not change width of dropdown to that of the activator
              hover: false, // Activate on hover
              gutter: 0, // Spacing from edge
              belowOrigin: true, // Displays dropdown below the button
              alignment: 'left', // Displays dropdown with edge aligned to the left of button
              stopPropagation: false // Stops event propagation
          });
        $(".sm-sidebar").sideNav();
        $(".brand-bars").click(function(){
            if ($(this).hasClass('opend')) {
                $(".sidebar").css({"width":"60px"});
                $(".side-menu-text").css({"display":"none"});
                $(".sidebar-logo-text").css({"display":"none"});
                $(".sidebar-logo").css({"display":"block"});
                $(".navbar-top").css({"width":"calc(100% - 60px)","margin-left":"60px"});
                $(".main").css({"width":"calc(100% - 60px)","margin-left":"60px"});
                $(this).removeClass('opend');
            }else{
                $(".navbar-top").css({"width":"calc(100% - 220px)","margin-left":"220px"});
                $(".main").css({"width":"calc(100% - 220px)","margin-left":"220px"});
                setTimeout(function (){
                  $(".side-menu-text").css({"display":"initial"});
                  $(".sidebar-logo-text").css({"display":"block"});
                  $(".sidebar-logo").css({"display":"block"});
                },200);
                $(".sidebar").css({"width":"220px"});

                $(this).addClass('opend');
            }
        });
  }
  $.fn.checkFileAll = function(){
    var tr_active = $(".tr-file.active").length;
    var tr = $(".tr-file").length;
    if (tr == tr_active) {
        $("#select-all").prop("checked",true);
    }else{
        $("#select-all").prop("checked",false);
    }
  }
 $.fn.preloader = function (){
    $(".digidoc-preload-backdrop").css({"display":"block"});
    setInterval(function(){hour() },2500);
    $(this).animateCss('fadeIn').css({"display":"block"})
 }
$.fn.closeloader = function (){
    $(".digidoc-preload-backdrop").css({"display":"none"});
    $(this).css({"display":"none"})
}
  function hour(){
    setTimeout(function(){
          $(".page-load").removeClass('fa-hourglass-start').addClass('fa-hourglass-half').css({"transform":"rotate(0deg"});
          $(".point-wait").html("..");
    },500);
    setTimeout(function(){
          $(".page-load").removeClass('fa-hourglass-half').addClass('fa-hourglass-end');
          $(".point-wait").html("...");
    },1000);
    setTimeout(function(){
          $(".page-load").removeClass('fa-hourglass-end').css({"transform":"rotate(360deg"});
          $(".point-wait").html("");
    },1500);
    setTimeout(function(){
          $(".page-load").addClass('fa-hourglass-start');
          $(".point-wait").html(".");
          // .addClass('fa-hourglass-start');
    },2000);
  }
  $.fn.digidocModal = function(options){
    // console.log($(this));
     var modal_id = $(this)[0].id;
     var bd = $(".digidoc-backdrop");
     var modal = $(this);
     if (typeof options["backdrop"] !== "undefined") {
         if (options["backdrop"]) {
             bd.css({"display":"block"});
         }
     }
     if (typeof options["static"] !== "undefined") {
         if (!options["static"]) {
              bdClose(bd,modal)
         }

     }else{
        bdClose(bd,modal)
     }

     $(".closemodal").click(function(){
         closeModal();
     });
     //
     function closeModal(){
         bd.css({"display":"none"});
         modal.remove();
     }
     //
     function bdClose(){
       $("#"+modal_id).click(function(e){
          if(e.target.id == modal_id){
            bd.css({"display":"none"});
            modal.remove();
          }
       });
     }
     modal.css({"display":"block"}).animateCss("fadeIn");
  }
})(jQuery);
