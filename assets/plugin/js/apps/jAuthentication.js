$(document).ready(function() {
  Getprofile();
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
     var connect = getUrl('');
     window.location = connect
  }
})

$(".auth-logout").click(function(event) {
    var api_url = getApi('logout');
    var token = localStorage.getItem('token');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":token},
      beforeSend:function(){
        preloader();
      }
    })
    .done(function(data) {
        closeloader()
        var obj = JSON.parse(data);
        if (obj.status == 200) {
          localStorage.clear();
          var connect = getUrl('');
          window.location = connect;
        }
    })
});

$("#profileUpload").change(function(event) {
  var files = $(this)[0].files;
  var type = files[0].name;
      type = type.split(".");
      type = type[type.length - 1];
      type = type.toLowerCase();
      if (type == "png" || type == "jpg" || type == "jpeg" || type == "gif" ) {
          var submitUrl = getApi('users/profileimg');
          var ajax = new XMLHttpRequest();
          var token = localStorage.getItem('token');
          var formData = new FormData();
              formData.append('file[]',files[0]);
          ajax.upload.addEventListener("progress",function(event){
              if(event.lengthComputable){
                  preloader();
                }
          });
          ajax.addEventListener("readystatechange",function(event){
            if(this.readyState ==4){
                if(this.status == 200){
                   var obj = JSON.parse(this.responseText);
                   if (obj.status == 200) {
                      location.reload();
                   }
              }
            }
          });
          ajax.open('POST',submitUrl);
          ajax.setRequestHeader("token",token);
          ajax.send(formData);
      }
});

function level(company) {
  var api_url = getApi('level/getdata');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:{"company_id":company},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader()
      data_level = JSON.parse(data);
      console.log(data_level);
  })
}

function Company() {
  var api_url = getApi('company/getdata');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader()
      data_company = JSON.parse(data);
      if (data_company.length > '0') {
        $(".company").removeAttr("disabled");
      }
      $(".company").html("<option value='' disabled selected>"+lang.request.company+"</option>");
      for (var i = 0; i < data_company.length; i++) {
        $(".company").append("<option value="+data_company[i]['company_id']+">"+data_company[i]['company_name_th']+" ("+data_company[i]['company_name_en']+")"+"</option>");
      }
  })
}
function Branch(company) {
  var api_url = getApi('branch/getdata');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:{"company_id":company},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader()
      data_branch = JSON.parse(data);
      if (data_branch.length > '0') {
        $(".branch").removeAttr("disabled");
      }
      $(".branch").html("<option value='' disabled selected>"+lang.request.branch+"</option>");
      for (var i = 0; i < data_branch.length; i++) {
        $(".branch").append("<option value="+data_branch[i]['branch_id']+">"+data_branch[i]['branch_name']+" ("+data_branch[i]['branch_code']+")"+"</option>");
      }
  })
}
function Dep(branch) {
  var api_url = getApi('dep/getdata');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:{"branch_id":branch},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      data_department = JSON.parse(data);
      if (data_department.length > '0') {
        $(".department").removeAttr("disabled")
      }
      $(".department").html("<option value='' disabled selected>"+lang.request.department+"</option>");
      for (var i = 0; i < data_department.length; i++) {
        $(".department").append("<option value="+data_department[i]['dep_id']+">"+data_department[i]['dep_name']+" ("+data_department[i]['dep_caption']+")"+"</option>");
      }
  })
}
function Site(dep) {
  var api_url = getApi('site/getdata');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:{"dep_id":dep},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader()
      data_site = JSON.parse(data);
      if (data_site.length > '0') {
        $(".site").removeAttr("disabled")
      }
      $(".site").html("<option value='' disabled selected>"+lang.request.site+"</option>");
      for (var i = 0; i < data_site.length; i++) {
        $(".site").append("<option value="+data_site[i]['site_id']+">"+data_site[i]['site_description']+" ("+data_site[i]['site_name']+")"+"</option>");
      }

  })
}
function Team(site) {
  var api_url = getApi('team/getdata');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:{"site_id":site},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader()
      data_team = JSON.parse(data);
      console.log(data_team);
      if (data_team.length > '0') {
        $(".team").removeAttr("disabled")
      }
      for (var i = 0; i < data_team.length; i++) {
        // $(".team").append("<option value="+data_team[i]['site_id']+">"+data_team[i]['site_description']+" ("+data_team[i]['site_name']+")"+"</option>");
      }
  })
}

function pageHome() {
  var connect = getUrl('home');
  window.location = connect;
  preloader();
}//end pageHome

function pageWorking() {
  var connect = getUrl('working');
  window.location = connect;
  preloader();
}//end pageHome

function pageRequest() {
  var connect = getUrl('request');
  window.location = connect;
  preloader();
}//end pageRequest

function pageTicket() {
  var connect = getUrl('ticket');
  window.location = connect;
  preloader();
}//end pageTicket

function pageReport() {
  var connect = getUrl('report');
  window.location = connect;
  preloader();
}//end pageReport

function pagereportsystem() {
  var connect = getUrl('reportsystem');
  window.location = connect;
  preloader();
}//end pageReportSystem

function pageAdministrator() {
  var connect = getUrl('administrator');
  window.location = connect;
  preloader();
}//end pageAdministrator

function pageSystem() {
  var connect = getUrl('systems');
  window.location = connect;
  preloader();
}//end pageSystem

function pageCreateCompany() {
  var connect = getUrl('createcompany');
  window.location = connect;
  preloader();
}//end pageCreateCompany

function pageCreateUser() {
  var connect = getUrl('createuser');
  window.location = connect;
  preloader();
}//end pageCreateUser

function pageInvite() {
  var connect = getUrl('administrator/inviteuser');
  window.location = connect;
  preloader();
}//end pageInvite

function pageViewUser() {
  var connect = getUrl('administrator/viewuser');
  window.location = connect;
  preloader();
}//end pageViewUser

function pageProfile(ele,id){
  var api_url = getApi('user/profiledetail');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"uid":id},
    beforeSend:function(){
      preloader();
    }
    })
    .done(function(data) {
      var obj = JSON.parse(data);
      console.log(obj);
      if (obj.status==200) {
        var connectURL = getUrl("profile");
        var formData = {"ajax":true};
            formData["data"] = obj['data'] ;
        $.ajax({
                url: connectURL,
                type: 'POST',
                data: formData
              }).done(function(data) {
                closeloader()
                var obj = JSON.parse(data);
                $(".modal-area").append(obj["modal"]);
                $("#ProfileDialog").bulletModal({"backdrop":true,"static":true});
                $('.tabs-member').tabs();
              })// load modal
      }else {
        closeloader()
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
      } //obj.status==200
    })//load data profile detail
}//end pageProfile
