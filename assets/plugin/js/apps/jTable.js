function genRowTicket(index,data,display) {
  var table = "";
  table += "<tr class='tr-ticket' id='tr_file_"+index+"' style='display:"+display+";' data-id='"+data.ticket_id+"' data-row='"+index+"'>";
  table += "<td>"+ticketstatus(data.ticket_status)+"</td>";
  table += "<td class='sort'>";
  table += "["+data.ticket_id+"] "+data.ticket_title+"<br><small> ขอโดย : "+data.fname+" "+data.lname+"</small>";
  table += "</td>";
  table += "<td>"+formatDate(data.ticket_date)+"</td>";
  table += "<td>"+ticketProgress(data.ticket_status)+"</td>";
  table += "</tr>";
  return table;
}

function getTrackingTicket(data) {
  var table = "";
  table += "<tr>"
  if (data.log_type == 'working' || data.log_type == 'checking') {
    table += "<td width='24px'>"
    table += "<img width='24px' src='"+data.active_image+"' >"
    table += "</td>"
    table += "<td>"
    table += data.active_name+" "+data.log_type_msg+" '"+data.ticket_title+"' ("+formatDate(data.log_date)+")"
    table += "</td>"
  }else if (data.log_type == 'assign') {
    table += "<td width='24px'>"
    table += "<img width='24px' src='"+data.passive_image+"' >"
    table += "</td>"
    table += "<td>"
    table += data.active_name+" "+data.log_type_msg+" '"+data.ticket_title+"' ให้ "+data.assign+" ("+formatDate(data.log_date)+")"
    table += "</td>"
  }else if (data.log_type == 'success') {
    table += "<td width='24px'>"
    table += "<i class='fas fa-check' style='font-size:24px'></i>"
    table += "</td>"
    table += "<td>"
    table += data.ticket_title+" "+data.log_type_msg+" ("+formatDate(data.log_date)+")<br>"
    table += "<small> ตรวจสอบโดย: "+data.work_QC_name+"</small>"
    table += "</td>"
  }else {
    table += "<td width='24px'>"
    table += "<img width='24px' src='"+data.active_image+"' >"
    table += "</td>"
    table += "<td>"
    table += data.active_name+" "+data.log_type_msg+" '"+data.ticket_title+"' ("+formatDate(data.log_date)+")"
    table += "</td>"
  }
  table += "</tr>"
  return table;
}

function getShowDetail(data) {
  var table = "";
  table += "<table><tr>"
  table += "<td width='24px'>"
  table += "<img width='24px' src='"+data.image+"' >"
  table += "</td>"
  table += "<td>"
  table += "<small>"+data.fname+" "+data.lname+"</small>"
  table += "<div class='work-details'>"
  table += nl2br(data.detail_description)
  table += "</div>"+"<small class='right'>"+formatDate(data.detail_date)+"</small>"
  table += "</td>"
  table += "</tr></table>"
  return table;
}

function ticketstatus(status) {
  var icon;
  switch (status) {
        case "1":
            icon = "<div class='chip chip-wait'>wait</div>";
            break;
        case "2":
            icon = "<div class='chip chip-wait'>assign</div>";
            break;
        case "3":
            icon = "<div class='chip chip-dismiss'>dismiss</div>";
            break;
        case "4":
            icon = "<div class='chip chip-wait'>working</div>";
            break;
        case "5":
            icon = "<div class='chip chip-wait'>check</div>";
            break;
        case "6":
            icon = "<div class='chip chip-dismiss'>cancel</div>";
            break;
        case "7":
            icon = "<div class='chip chip-success'>success</div>";
            break;
        default:
            icon = "<div class='chip chip-error'>error</div>";
      }
  return icon;
}

function ticketProgress(status) {
  var progress;
  switch (status) {
        case "1":
            progress = "<div class='progress wait'><div class='determinate' style='width:20%'></div></div>";
            progress += "<small>20% (4 more steps)</small>";
            break;
        case "2":
            progress = "<div class='progress assign'><div class='determinate' style='width:40%'></div></div>";
            progress += "<small>40% (3 more steps)</small>";
            break;
        case "4":
            progress = "<div class='progress working'><div class='determinate' style='width:60%'></div></div>";
            progress += "<small>60% (2 more steps)</small>";
            break;
        case "5":
            progress = "<div class='progress check'><div class='determinate' style='width:80%'></div></div>";
            progress += "<small>80% (1 more steps)</small>";
            break;
        case "7":
            progress = "<div class='progress success'><div class='determinate' style='width:100%'></div></div>";
            progress += "<small>100% (success)</small>";
            break;
        default:
            progress = "<div class='progress error'><div class='determinate' style='width:0%'></div></div>";
      }
  return progress;
}

function formatDate(date) {
  var datetime = new Date(date);
  var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
  var hours = datetime.getHours();
      hours = hours < 10 ? '0'+hours : hours;
  var minutes = datetime.getMinutes();
      minutes = minutes < 10 ? '0'+minutes : minutes;
  var day = datetime.getDate();
  var monthIndex = datetime.getMonth();
  var year = datetime.getFullYear();
  var times = hours+':'+minutes
  return day + ' ' + monthNames[monthIndex] + ' ' + year+' '+times;
}
