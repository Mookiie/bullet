$(document).ready(function() {
  closeloader()
})

$("form#loginform").submit(function(ev){
    ev.preventDefault();
    var dataform = { "username":$('#username').val(),
                     "password":md5($('#password').val()),
                   };
    var api_url = getApi("login");
    $.ajax({
       url: api_url,
       type: 'POST',
       data: dataform,
       beforeSend:function(){
         preloader();
       }
     })
     .done(function(data) {
       closeloader()
       var obj = JSON.parse(data);
       if (obj.status==200) {
         $(".loader").show();
         var autoken = obj["token"];
         var connect = getUrl('home');
         localStorage.setItem("token",autoken);
         localStorage.setItem('channel',obj.channel);
         window.location = connect;
         preloader();
       }else if (obj.status==402) {
         addErr("password",lang.error.login_password);
       }else if (obj.status==501) {
         addErr("username",lang.error.login_waitconfirm);
       }else if (obj.status==502) {
         addErr("username",lang.error.login_noactive);
       }else if (obj.status==404) {
         addErr("username",lang.error.login_notfound);
       }

     })

  })

$("#loginform").keypress(function(event) {
    var code = event.keyCode;
    if (code == 13) {
        $(".btn#login").trigger('click');
    }
});
