var company_id = "";
$(document).ready(function(){
  closeloader();
  ticketWork(company_id,'(2,4,5,7)')
  CountWorkStatus("count-assign",company_id,"(1,2)")
  CountWorkStatus("count-working",company_id,"(4)")
  CountWorkStatus("count-check",company_id,"(5)")
  CountWorkStatus("count-success",company_id,"(3,6,7)");
  CountWorkStatus("count-record",company_id,'');
})
$("#company").change(function() {
  company_id = $(this).val();
  CountWorkStatus("count-assign",company_id,"(1,2)");
  CountWorkStatus("count-working",company_id,"(4)");
  CountWorkStatus("count-check",company_id,"(5)");
  CountWorkStatus("count-success",company_id,"(3,6,7)");
  CountWorkStatus("count-record",company_id,"");
  ticketWork(company_id,'(2,4,5,7)');
})

function CountWorkStatus(id,company,status) {
  var api_url = getApi('ticket/countwork');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"company_id":company, "status":status}
  })
  .done(function(data) {
      var obj = JSON.parse(data);
      $("#"+id).html(obj);
  })
}

function nextpage(el){
  console.log("nextpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var next = $("#nowpage")[0].innerHTML;
      next = parseFloat(next);
      next ++;
  var start = (next-1) * has;
  if (next <= parseFloat($(".totalpage")[0].innerHTML)) {
             $("#nowpage").html(next);
             $(".file-container").html("")
             $(".file-container").getTables({"data" : work_me ,"page": start , "view" : $("#row_show").val() });
   }
}

function prevpage(el) {
  console.log("prevpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var prev = $("#nowpage")[0].innerHTML;
      prev = parseFloat(prev);
      prev --;
  var start = (prev-1) * has;
  if (prev >  0) {
              $("#nowpage").html(prev);
              $(".file-container").html("")
              $(".file-container").getTables({"data" : work_me ,"page": start , "view" : $("#row_show").val() });
  }
}

$.fn.getTables = function (obj){
    var view = obj["view"];
        view = parseFloat(view);
    var data = obj["data"];
    var page = obj["page"];
    var row = data.length;
    for (var i = 0; i < data.length; i++) {
         var display = "";
         if (i <  (page)) {
             display = "none";
         }
         if (i > (view - 1)+page) {
             display = "none";
         }
         var row = genRowTicket(i,data[i],display);
         $(this).append(row);
    }
    var has = 0;
        var tr = $(".file-container tr");
        tr.each(function(index, el) {
            if($(el).css('display') != "none"){
               has++;
            }
        });
    var total = Math.ceil((tr.length)/view);
    $(".totalpage").html(total);

    $('.tr-ticket').click(function() {
      console.log($(this).data('id'));
      var ticket_id =$(this).data('id');
      var api_url = getApi('ticket/profile');
      var token = localStorage.getItem('token');
      var obj_ticket;
      $.ajax({
        url: api_url,
        type: 'GET',
        headers:{"token":token},
        data :{"ticket_id":ticket_id},
        beforeSend:function(){
          preloader();
        }
        })
        .done(function(data) {
          obj_ticket = JSON.parse(data);
            console.log(obj_ticket['data']);
            if (obj_ticket.status==200) {
              var connectURL = getUrl("ticket/ticketDialog");
              var formData = {"ajax":true};
                  formData["data"] = obj_ticket['data'] ;
              $.ajax({
                      url: connectURL,
                      type: 'POST',
                      data: formData
                    }).done(function(data) {
                      closeloader()
                      var obj_modal = JSON.parse(data);
                      $(".modal-area").append(obj_modal["modal"]);
                      $("#TicketDialog").bulletModal({"backdrop":true,"static":true});
                      $(".tabs-member").tabs();
                      $("#work_id").val(formData.data.work_id);
                      // profile-ticket
                      $(".work-menu").css("display","");
                      $("#working").click(function () {
                        var api_url = getApi('ticket/working');
                        var token = localStorage.getItem('token');
                        $.ajax({
                                url: api_url,
                                type: 'GET',
                                headers:{"token":token},
                                data: {"ticket_id":ticket_id},
                                beforeSend:function(){
                                  preloader();
                                }
                              }).done(function(data) {
                                closeloader()
                                var obj = JSON.parse(data);
                                if (obj.status == 200) {
                                  swal({
                                         title: 'Success!',
                                         text: "",
                                         type: 'success',
                                         showCancelButton: false,
                                         confirmButtonColor: '#3085d6',
                                         confirmButtonText: 'OK!'
                                      }).then((result) => {
                                         if (result.value) {
                                          location.reload();
                                         }
                                     })
                                }
                                else {
                                  Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                                }
                              })
                      })
                      $("#dismiss-w").click(function () {
                        var api_url = getApi('ticket/dismisswork');
                        var token = localStorage.getItem('token');
                        $.ajax({
                                url: api_url,
                                type: 'GET',
                                headers:{"token":token},
                                data: {"ticket_id":ticket_id},
                                beforeSend:function(){
                                  preloader();
                                }
                              }).done(function(data) {
                                closeloader()
                                var obj = JSON.parse(data);
                                if (obj.status == 200) {
                                  swal({
                                         title: 'Success!',
                                         text: "",
                                         type: 'success',
                                         showCancelButton: false,
                                         confirmButtonColor: '#3085d6',
                                         confirmButtonText: 'OK!'
                                      }).then((result) => {
                                         if (result.value) {
                                          location.reload();
                                         }
                                     })
                                }
                                else {
                                  Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                                }
                              })
                      })
                      // ticket_attached
                      if (formData.data.ticket_image != '') {
                        $(".attached").css("display","");
                        var pathtxt = $("#pathtxt").val();
                         if(pathtxt != ""){
                           PDFObject.embed(pathtxt, "#framPDF");
                         }
                        $("#download").click(function () {
                          var api_url = getApi('ticket/download');
                          var token = localStorage.getItem('token');
                          $.ajax({
                                  url: api_url,
                                  type: 'POST',
                                  headers:{"token":token},
                                  data: {"ticket_id":ticket_id}
                                }).done(function(data) {

                                })
                        })
                      }
                      // tracking
                      if (formData.data.ticket_status == 4||formData.data.ticket_status == 5||formData.data.ticket_status == 7) {
                        $(".detail").css("display","");
                        $(".work-menu").css("display","none");
                        $(".detail").click(function () {
                          getDetailWork(ticket_id);
                        })
                      }
                      $("#tracking").click(function () {
                        var api_url = getApi('ticket/tracking');
                        var token = localStorage.getItem('token');
                        $.ajax({
                          url: api_url,
                          type: 'GET',
                          headers:{"token":token},
                          data :{"ticket_id":ticket_id},
                          beforeSend:function(){
                            preloader();
                          }
                          })
                          .done(function(data) {
                            closeloader()
                            var obj = JSON.parse(data);
                            console.log(obj);
                            if (obj.status == 200) {
                              var tracking_table = '';
                                for (var i = 0; i < obj.data.length; i++) {
                                  var tracking = obj.data[i]
                                  tracking_table += getTrackingTicket(tracking);
                                  $("#tracking_list").html(tracking_table);
                                }
                            }else {
                              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                            }

                          })
                      })
                      if (formData.data.ticket_status == 4) {
                        $(".detail-menu").css("display","");
                        $("#detail").click(function () {
                          $(".add-detail").css("display","");
                        })
                        $("#close-add").click(function () {
                          $("#file").val("");
                          $("#nfile").val("");
                          $("#workingdetail").val("");
                          $(".add-detail").css("display","none");
                        })

                        $("#success").click(function () {
                          $(".detail-menu").css("display","none");
                          var api_url = getApi('ticket/working_success');
                          var token = localStorage.getItem('token');
                          $.ajax({
                            url: api_url,
                            type: 'GET',
                            headers:{"token":token},
                            data :{"ticket_id":ticket_id},
                            beforeSend:function(){
                              preloader();
                            }
                          }).done(function(data) {
                            var obj = JSON.parse(data);
                            if (obj.status == 200) {
                              closeloader()
                              swal({
                                     title: 'Success!',
                                     text: "",
                                     type: 'success',
                                     showCancelButton: false,
                                     confirmButtonColor: '#3085d6',
                                     confirmButtonText: 'OK!'
                                  }).then((result) => {
                                     if (result.value) {
                                       // getDetailWork(ticket_id);
                                       CountWorkStatus("count-working",company_id,"(4)")
                                       CountWorkStatus("count-check",company_id,"(5)")
                                       $(".closemodal").trigger("click");
                                       ticketWork(company_id,'(2,4,5,7)')
                                     }
                                 })
                            }
                          })
                        })
                        $("#send-add").click(function(){
                          var dataform = { "ticket_id":ticket_id,
                                           "work_id":$("#work_id").val(),
                                           "detail_description":$("#workingdetail").val()
                                         };
                            var api_url = getApi('ticket/detail');
                            var token = localStorage.getItem('token');
                            if (isType($("#nfile").val())||document.getElementById("file").files.length !== 0) {
                              $.ajax({
                                url: api_url,
                                type: 'POST',
                                headers:{"token":token},
                                data : dataform,
                                beforeSend:function(){
                                  preloader();
                                }
                              })
                               .done(function(data) {
                                var obj = JSON.parse(data);
                                if (obj.status == 200) {
                                  if (document.getElementById("file").files.length !== 0) {
                                    upload_file(ticket_id,obj['id'],ticket_id)
                                  }else {
                                    closeloader()
                                    swal({
                                           title: 'Success!',
                                           text: "",
                                           type: 'success',
                                           showCancelButton: false,
                                           confirmButtonColor: '#3085d6',
                                           confirmButtonText: 'OK!'
                                        }).then((result) => {
                                           if (result.value) {
                                             // location.reload();
                                             getDetailWork(ticket_id);
                                             $("#close-add").trigger("click");
                                           }
                                       })
                                    }
                                }
                              })
                            }else {
                              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.errortype, 4000);
                            }
                        })//from submit
                        function upload_file(id,detail_id,ticket_id) {
                          var files = $("#file")[0].files;
                          var type = files[0].name;
                              type = type.split(".");
                              type = type[type.length - 1];
                              type = type.toLowerCase();
                          var submitUrl = getApi('ticket/detail_file');
                          var ajax = new XMLHttpRequest();
                          var token = localStorage.getItem('token');
                          var formData = new FormData();
                              formData.append('file[]',files[0]);
                              formData.append('id',id)
                              formData.append('detail_id',detail_id)
                          ajax.upload.addEventListener("progress",function(event){
                              if(event.lengthComputable){
                                  //
                                }
                          });
                          ajax.addEventListener("readystatechange",function(event){
                            if(this.readyState ==4){
                                if(this.status == 200){
                                  closeloader()
                                   var obj = JSON.parse(this.responseText);
                                   if (obj.status == 200) {
                                     swal({
                                            title: 'Success!',
                                            text: "",
                                            type: 'success',
                                            showCancelButton: false,
                                            confirmButtonColor: '#3085d6',
                                            confirmButtonText: 'OK!'
                                         }).then((result) => {
                                            if (result.value) {
                                              // location.reload();
                                              getDetailWork(ticket_id);
                                              $("#close-add").trigger("click");
                                            }
                                        })//swal
                                  }//obj.status == 200
                                }//this.status == 200
                              }//this.readyState ==4
                            });
                          ajax.open('POST',submitUrl);
                          ajax.setRequestHeader("token",token);
                          ajax.send(formData);
                        }
                      }
                      if (formData.data.ticket_status == 5) {
                        if (profile_me.data.level.level_qc == 1) {
                          console.log(ticket_id);
                          $(".qc-menu").css("display","");
                        }// profile_me.data.level.level_assign == 1
                        $("#pass").click(function () {
                          var api_url = getApi('ticket/success');
                          var token = localStorage.getItem('token');
                          $.ajax({
                            url: api_url,
                            type: 'GET',
                            headers:{"token":token},
                            data :{"ticket_id":ticket_id},
                            beforeSend:function(){
                              preloader();
                            }
                          }).done(function(data) {
                            var obj = JSON.parse(data);
                            if (obj.status == 200) {
                              closeloader()
                              swal({
                                     title: 'Success!',
                                     text: "",
                                     type: 'success',
                                     showCancelButton: false,
                                     confirmButtonColor: '#3085d6',
                                     confirmButtonText: 'OK!'
                                  }).then((result) => {
                                     if (result.value) {
                                       $(".qc-menu").css("display","none");
                                       $(".closemodal").trigger("click")
                                       CountWorkStatus("count-check",company_id,"(5)")
                                       CountWorkStatus("count-success",company_id,"(3,6,7)");
                                       ticketWork(company_id,'(2,4,5,7)')
                                     }
                                 })
                            }
                          })

                        })
                        $("#fail").click(function () {
                          var api_url = getApi('ticket/eject');
                          var token = localStorage.getItem('token');
                          swal({
                                title: 'เหตุผลที่ไม่ผ่าน...',
                                input: 'text',
                                showCancelButton: true,
                                inputValidator: (value) => {
                                  return !value && 'กรุณากรอกเหตุผล!'
                                }
                              }).then((result) => {
                                if (result.value) {
                                  console.log(result.value);
                                  console.log(ticket_id);
                                  $.ajax({
                                    url: api_url,
                                    type: 'GET',
                                    headers:{"token":token},
                                    data :{"ticket_id":ticket_id,
                                           "reason":result.value},
                                    beforeSend:function(){
                                      preloader();
                                    }
                                  }).done(function(data) {
                                      closeloader();
                                      var obj = JSON.parse(data);
                                        if (obj.status == 200) {
                                          swal({
                                                 title: 'Success!',
                                                 text: "",
                                                 type: 'success',
                                                 showCancelButton: false,
                                                 confirmButtonColor: '#3085d6',
                                                 confirmButtonText: 'OK!'
                                              }).then((result) => {
                                                 if (result.value) {
                                                   $(".qc-menu").css("display","none");
                                                   $(".closemodal").trigger("click")
                                                   CountWorkStatus("count-check",company_id,"(5)")
                                                   CountWorkStatus("count-success",company_id,"(3,6,7)");
                                                   ticketWork(company_id,'(2,4,5,7)')
                                                 }
                                             })
                                        }
                                  })
                                }
                              })// swal
                        })
                      }
                      if (formData.data.ticket_status > 1) {
                        $(".dialogservice").css("display","");
                      }
                  })// load modal
            }else {
              closeloader()
              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
            } //obj.status==200
        }) // load data ticket/detail
    })//$('.tr-ticket').click
  }

function getviewTables(ele){
  var view = ele.val();
  $(".file-container").html("")
  $(".file-container").getTables({"data" : work_me ,"page": 0 , "view" : $("#row_show").val() });
  $("#nowpage").html(1);
}

function searchInTables(ele){
  var filter = ele.val().toUpperCase();
  var filter_obj = ticket_me;
  var tr = $(".file-container tr");
  for (var r = 0; r < tr.length; r++) {
    var row = tr[r].dataset.row;
    var td = $("#tr_file_"+row+" td.sort");
    var str = "";
      for (var i = 0; i < td.length; i++) {
        str += td[i].innerHTML;
      }
      if(str.toUpperCase().indexOf(filter) > -1){
        $("#tr_file_"+row).css({"display":""});
      }else{
        $("#tr_file_"+row).css({"display":"none"});
      }
  }
}

function ticket_assign() {
  ticketWork(company_id,"(2)")
}
function ticket_working() {
  ticketWork(company_id,"(4)")
}
function ticket_check() {
  ticketWork(company_id,"(5)")
}
function ticket_success() {
  ticketWork(company_id,"(7)")
}
