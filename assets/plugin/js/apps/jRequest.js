$(document).ready(function() {
  closeloader();
  Branch();
})
$("#company").change(function() {
  Branch($(this).val());
  $(".branch").html("<option value='' disabled selected>"+lang.request.branch+"</option>");
})

$("#branch").change(function() {
  Dep($(this).val())
  $(".site").attr("disabled", 'disabled');
  $(".site").html("<option value='' disabled selected>"+lang.request.site+"</option>");
  $(".team").attr("disabled", 'disabled');
  $(".team").html("<option value='' disabled selected>"+lang.request.team+"</option>");
})

$("#dep").change(function() {
  Site($(this).val())
  $(".team").attr("disabled", 'disabled');
  $(".team").html("<option value='' disabled selected>"+lang.request.team+"</option>");
})

$("form#formTicket").submit(function(ev){
    ev.preventDefault();
    var dataform = { "ticket_company":$('#company').val(),
                     "ticket_branch":$('#branch').val(),
                     "ticket_dep":$('#dep').val(),
                     "ticket_site":$('#site').val(),
                     "ticket_title":$('#title').val(),
                     "ticket_detail":$('#detail').val(),
                   };
    var api_url = getApi('ticket/create_ticket');
    var token = localStorage.getItem('token');
    if (isType($("#nfile").val()) || document.getElementById("file").files.length !== 0) {
      $.ajax({
        url: api_url,
        type: 'POST',
        headers:{"token":token},
        data : dataform,
        beforeSend:function(){
          preloader();
        }
      })
      .done(function(data) {
        var obj = JSON.parse(data);
        if (obj.status == 200) {
          if (document.getElementById("file").files.length !== 0) {
            upload_file(obj['id'])
          }else {
            closeloader()
            swal({
                   title: 'Success!',
                   text: "",
                   type: 'success',
                   showCancelButton: false,
                   confirmButtonColor: '#3085d6',
                   confirmButtonText: 'OK!'
                }).then((result) => {
                   if (result.value) {
                     location.reload();
                   }
               })
            }
        }
      })
    }else {
      Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.errortype, 4000);
    }

})


function upload_file(id) {
  var files = $("#file")[0].files;
  var type = files[0].name;
      type = type.split(".");
      type = type[type.length - 1];
      type = type.toLowerCase();
  var submitUrl = getApi('ticket/ticket_files');
  var ajax = new XMLHttpRequest();
  var token = localStorage.getItem('token');
  var formData = new FormData();
      formData.append('file[]',files[0]);
      formData.append('id',id)
  ajax.upload.addEventListener("progress",function(event){
      if(event.lengthComputable){
          //
        }
  });
  ajax.addEventListener("readystatechange",function(event){
    if(this.readyState ==4){
        if(this.status == 200){
          closeloader()
           var obj = JSON.parse(this.responseText);
           if (obj.status == 200) {
             swal({
                    title: 'Success!',
                    text: "",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                 }).then((result) => {
                    if (result.value) {
                      location.reload();
                    }
                })
           }
      }
    }
  });
  ajax.open('POST',submitUrl);
  ajax.setRequestHeader("token",token);
  ajax.send(formData);
}
