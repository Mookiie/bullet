
$(document).ready(function() {

  CountTicketStatus("count-request","(1,2)");
  CountTicketStatus("count-working","(4,5)");
  CountTicketStatus("count-success","(3,6,7)");
  CountTicketStatus("count-record","");
  //count-request = wait	1
  //count-wait = assign	2
  // eject	3
  // count-working = working	4
  // count-check = check	5
  // cancel	6
  // count-success = success	7
  ticketme("")
})

function CountTicketStatus(id,status) {
  var api_url = getApi('ticket/countstatusme');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data :{"status":status}
  })
  .done(function(data) {
      var obj = JSON.parse(data);
      $("#"+id).html(obj);
  })
}

function nextpage(el){
  console.log("nextpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var next = $("#nowpage")[0].innerHTML;
      next = parseFloat(next);
      next ++;
  var start = (next-1) * has;
  if (next <= parseFloat($(".totalpage")[0].innerHTML)) {
             $("#nowpage").html(next);
             $(".file-container").html("")
             $(".file-container").getTables({"data" : ticket_me ,"page": start , "view" : $("#row_show").val() });
   }
}

function prevpage(el) {
  console.log("prevpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var prev = $("#nowpage")[0].innerHTML;
      prev = parseFloat(prev);
      prev --;
  var start = (prev-1) * has;
  if (prev >  0) {
              $("#nowpage").html(prev);
              $(".file-container").html("")
              $(".file-container").getTables({"data" : ticket_me ,"page": start , "view" : $("#row_show").val() });
  }
}

$.fn.getTables = function (obj){
    var view = obj["view"];
        view = parseFloat(view);
    var data = obj["data"];
    var page = obj["page"];
    var row = data.length;
    for (var i = 0; i < data.length; i++) {
         var display = "";
         if (i <  (page)) {
             display = "none";
         }
         if (i > (view - 1)+page) {
             display = "none";
         }
         var row = genRowTicket(i,data[i],display);
         $(this).append(row);
    }
    var has = 0;
        var tr = $(".file-container tr");
        tr.each(function(index, el) {
            if($(el).css('display') != "none"){
               has++;
            }
        });
    var total = Math.ceil((tr.length)/view);
    $(".totalpage").html(total);

    $('.tr-ticket').click(function() {
      console.log($(this).data('id'));
      var ticket_id = $(this).data('id');
      var api_url = getApi('ticket/profile');
      var token = localStorage.getItem('token');
      $.ajax({
        url: api_url,
        type: 'GET',
        headers:{"token":token},
        data :{"ticket_id":ticket_id},
        beforeSend:function(){
          preloader();
        }
        })
        .done(function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj.status==200) {
              var connectURL = getUrl("ticket/ticketDialog");
              var formData = {"ajax":true};
                  formData["data"] = obj['data'] ;
              $.ajax({
                      url: connectURL,
                      type: 'POST',
                      data: formData
                    }).done(function(data) {
                      closeloader()
                      var obj = JSON.parse(data);
                      $(".modal-area").append(obj["modal"]);
                      $("#TicketDialog").bulletModal({"backdrop":true,"static":true});
                      $('.tabs-member').tabs();
                      if (formData.data.ticket_status == 4||formData.data.ticket_status == 5||formData.data.ticket_status == 7) {
                        $(".detail").css("display","");
                      }

                      if (formData.data.ticket_status == 1) {
                        $(".user-menu").css("display","");
                      }
                      if (formData.data.ticket_status > 1) {
                        $(".dialogservice").css("display","");
                      }
                      $("#edit").click(function() {
                        $("a#edit_config").trigger("click")
                      })
                      $("form#formEditTicket").submit(function(ev){
                        ev.preventDefault();
                        console.log("edit");
                        var dataform = { "ticket_id":ticket_id,
                                         "ticket_title":$('#title-edit').val(),
                                         "ticket_detail":$('#detail-edit').val(),
                                       };
                        console.log(dataform);
                        var api_url = getApi('ticket/edit_ticket');
                        var token = localStorage.getItem('token');
                        if (isType($("#nfile").val()) || document.getElementById("file").files.length !== 0) {
                          $.ajax({
                            url: api_url,
                            type: 'POST',
                            headers:{"token":token},
                            data : dataform,
                            beforeSend:function(){
                              preloader();
                            }
                          })
                          .done(function(data) {
                            var obj = JSON.parse(data);
                            if (obj.status == 200) {
                              if (document.getElementById("file").files.length !== 0) {
                                upload_file(obj['id'])
                              }else {
                                closeloader()
                                swal({
                                       title: 'Success!',
                                       text: "",
                                       type: 'success',
                                       showCancelButton: false,
                                       confirmButtonColor: '#3085d6',
                                       confirmButtonText: 'OK!'
                                    }).then((result) => {
                                       if (result.value) {
                                         location.reload();
                                       }
                                   })
                                }
                            }
                          })
                        }else {
                          Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.errortype, 4000);
                        }

                      })
                      $("#dismiss-u").click(function () {
                        var api_url = getApi('ticket/dismissuser');
                        var token = localStorage.getItem('token');
                        $.ajax({
                                url: api_url,
                                type: 'GET',
                                headers:{"token":token},
                                data: {"ticket_id":ticket_id},
                                beforeSend:function(){
                                  preloader();
                                }
                              }).done(function(data) {
                                closeloader()
                                var obj = JSON.parse(data);
                                if (obj.status == 200) {
                                  swal({
                                         title: 'Success!',
                                         text: "",
                                         type: 'success',
                                         showCancelButton: false,
                                         confirmButtonColor: '#3085d6',
                                         confirmButtonText: 'OK!'
                                      }).then((result) => {
                                         if (result.value) {
                                          location.reload();
                                         }
                                     })
                                }
                                else {
                                  Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                                }
                              })
                      })

                      // ticket_attached
                      if (formData.data.ticket_image != '') {
                        $(".attached").css("display","");
                        var pathtxt = $("#pathtxt").val();
                         if(pathtxt != ""){
                           PDFObject.embed(pathtxt, "#framPDF");
                         }
                        $("#download").click(function () {
                          var api_url = getApi('ticket/download');
                          var token = localStorage.getItem('token');
                          $.ajax({
                                  url: api_url,
                                  type: 'POST',
                                  headers:{"token":token},
                                  data: {"ticket_id":ticket_id}
                                }).done(function(data) {

                                })
                        })
                      }
                      if (formData.data.ticket_status == 4||formData.data.ticket_status == 5||formData.data.ticket_status == 7) {
                        $(".detail").css("display","");
                        $(".work-menu").css("display","none");
                        $(".detail").click(function () {
                          console.log("detail");
                          getDetailWork(ticket_id);
                        })
                      }
                      if (formData.data.ticket_status == 6) {
                        $(".reticket-menu").css("display","");
                      }
                      $("#tracking").click(function () {
                        var api_url = getApi('ticket/tracking');
                        var token = localStorage.getItem('token');
                        $.ajax({
                          url: api_url,
                          type: 'GET',
                          headers:{"token":token},
                          data :{"ticket_id":ticket_id},
                          beforeSend:function(){
                            preloader();
                          }
                          })
                          .done(function(data) {
                            closeloader()
                            var obj = JSON.parse(data);
                            if (obj.status == 200) {
                              var tracking_table = '';
                                for (var i = 0; i < obj.data.length; i++) {
                                  var tracking = obj.data[i]
                                  tracking_table += getTrackingTicket(tracking);
                                  $("#tracking_list").html(tracking_table);
                                }
                            }else {
                              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
                            }

                          })
                      })
                    })// load modal
            }else {
              closeloader()
              Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.error, 4000);
            } //obj.status==200
        }) // load data ticket/detail
    })//$('.tr-ticket').click

  }

function getviewTables(ele){
  var view = ele.val();
  $(".file-container").html("")
  $(".file-container").getTables({"data" : ticket_me ,"page": 0 , "view" : $("#row_show").val() });
  $("#nowpage").html(1);
}

function searchInTables(ele){
  var filter = ele.val().toUpperCase();
  var filter_obj = ticket_me;
  var tr = $(".file-container tr");
  for (var r = 0; r < tr.length; r++) {
    var row = tr[r].dataset.row;
    var td = $("#tr_file_"+row+" td.sort");
    var str = "";
      for (var i = 0; i < td.length; i++) {
        str += td[i].innerHTML;
      }
      if(str.toUpperCase().indexOf(filter) > -1){
        $("#tr_file_"+row).css({"display":""});
      }else{
        $("#tr_file_"+row).css({"display":"none"});
      }
  }
}

function ticket_wait() {
  ticketme("(1,2)")
}
function ticket_working() {
  ticketme("(4,5)")
}
function ticket_success() {
  ticketme("(3,6,7)")
}

function upload_file(id) {
  var files = $("#file")[0].files;
  var type = files[0].name;
      type = type.split(".");
      type = type[type.length - 1];
      type = type.toLowerCase();
  var submitUrl = getApi('ticket/ticket_files');
  var ajax = new XMLHttpRequest();
  var token = localStorage.getItem('token');
  var formData = new FormData();
      formData.append('file[]',files[0]);
      formData.append('id',id)
  ajax.upload.addEventListener("progress",function(event){
      if(event.lengthComputable){
          //
        }
  });
  ajax.addEventListener("readystatechange",function(event){
    if(this.readyState ==4){
        if(this.status == 200){
          closeloader()
           var obj = JSON.parse(this.responseText);
           if (obj.status == 200) {
             swal({
                    title: 'Success!',
                    text: "",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                 }).then((result) => {
                    if (result.value) {
                      location.reload();
                    }
                })
           }
      }
    }
  });
  ajax.open('POST',submitUrl);
  ajax.setRequestHeader("token",token);
  ajax.send(formData);
}




  // $(".counter").counterUp({
  //         delay: 100,
  //         time: 1200
  // });
