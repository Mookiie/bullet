<?php

  if (!defined("BASEPATH")) exit('No direct script access allowed');
  date_default_timezone_set("Asia/Bangkok");

  class MY_Controller extends CI_Controller{

        var $template = array();
        var $data = array();
        var $language = "thai";
        // var $storage_path = "http://203.154.39.51/storage/";

        public function __construct(){
           parent::__construct();
        }

        public function layout(){
            $this->template["middle"] = $this->load->view($this->middle,$this->data,true);
            $this->load->view("layout/index",$this->template);
        }

        public function layouts(){
            $this->template["middle"] = $this->load->view($this->middle,$this->data,true);
            $this->load->view("layouts/index",$this->template);
        }

        public function formLayouts(){
          // print_r($this->middle);exit;
          $this->template["middle"] = $this->load->view($this->middle,$this->data,true);
          $this->load->view("layouts/forms",$this->template);
        }

        public function formGuestLayouts(){
          $this->template["middle"] = $this->load->view($this->middle,$this->data,true);
          $this->load->view("layouts/guest",$this->template);
        }

  }

?>
