<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/request/index';
    $this->data["title"]  = "Request";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jRequest.js")."'></script>";
    $this->layouts();
  }

}
