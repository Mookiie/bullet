<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/admin/index';
    $this->data["title"]  = "Report";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAdmin.js")."'></script>";
    $this->layouts();
  }
  public function Inviteuser()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/admin/invite';
    $this->data["title"]  = "Report";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAdmin.js")."'></script>";
    $this->layouts();
  }
  public function Viewuser()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/admin/viewuser';
    $this->data["title"]  = "Report";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAdmin.js")."'></script>";
    $this->layouts();
  }

}
