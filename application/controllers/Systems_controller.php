<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Systems_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/systems/index';
    $this->data["title"]  = "Systems";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jSystems.js")."'></script>";
    $this->layouts();
  }

}
