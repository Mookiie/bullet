<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/report/index';
    $this->data["title"]  = "Report";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jReport.js")."'></script>";
    $this->layouts();
  }
  public function reportsystem()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/report/system';
    $this->data["title"]  = "Report";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jReportSystem.js")."'></script>";
    $this->layouts();
  }

}
