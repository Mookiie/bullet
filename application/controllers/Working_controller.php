<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Working_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/working/index';
    $this->data["title"]  = "Working";
    $this->data["css"]  = "";
    $this->data["js"]   = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jWorking.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

}
