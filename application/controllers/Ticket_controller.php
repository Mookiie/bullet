<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/ticket/index';
    $this->data["title"]  = "Ticket";
    $this->data["css"]  = "";
    $this->data["js"]   = "";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTicket.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

  public function getTicketDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["ticket"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("bullet/ticket/ticketdialog",$data,true);
				}
		}

		echo json_encode($callback);
		return;
  }

}
