<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'bullet/dashboard/index';
    $this->data["title"]  = "Dashboard";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jHome.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

  public function getProfileDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("bullet/dashboard/profiledialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }
  public function getTicketDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["ticket"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("bullet/dashboard/myticketdialog",$data,true);
				}
		}

		echo json_encode($callback);
		return;
  }


}
