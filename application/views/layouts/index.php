<!DOCTYPE html>
  <html>
  <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/materialize/css/materialize.min.css"); ?>"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/animate.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/layouts.css"); ?>"/>
      <!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/layout.css"); ?>"/> -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/global.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/sweetalert2/dist/sweetalert2.min.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/font-awesome-animation.min.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/datepicker/dist/datepicker.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/timepicker/css/timepicki.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/bower_components/sidebar-nav/dist/sidebar-nav.min.css")?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/bower_components/toast-master/css/jquery.toast.css")?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/bower_components/morrisjs/morris.css")?>" />
      <?php if (isset($css)){echo $css;} ?>
      <link rel="icon" href="<?php echo base_url("assets/images/logo/logo.png") ?>" type="image/gif" sizes="16x16">
      <title id='app_title'> BULLET - <?php if (isset($title)){echo $title;} ?></title>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
      <!-- <script type="text/javascript" src="<?php echo base_url("assets/plugin/fontawesome/svg-with-js/js/fontawesome-all.min.js"); ?>"></script> -->
      <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/jquery-3.2.1.min.js"); ?>"></script>
      <script type="text/javascript" src="<?php echo base_url("assets/plugin/materialize/js/materialize.min.js"); ?>"></script>
      <script type='text/javascript' src='<?php echo base_url("assets/plugin/sweetalert2/dist/sweetalert2.all.min.js"); ?>'></script>

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body >
    <ul id="slide-out" class="side-nav">
      <li>
        <div class="user-view  get-profile" onclick="pageProfile($(this),'')">
        <div class="background"></div>
        <a ><img class="circle auth_profile" ></a>
        <a ><span class="name"></span></a>
        <a ><span class="email"></span></a>
      </div></li>
      <div class="normal_sidebar">
        <li data-href='home' class='home' id='homepage' onclick="pageHome()" >
          <a><i class='fas fa-home'></i> <span><?php echo $this->lang->line('nav_requestmepage'); ?></span></a>
        </li>
        <li data-href='work' class='work' id='workpage' onclick="pageWorking()" >
          <a><i class='fas fa-briefcase'></i> <span><?php echo $this->lang->line('nav_workmepage'); ?></span></a>
        </li>
        <li data-href='request' class='request' id='requestpage' onclick="pageRequest()" >
          <a><i class='fas fa-file-alt'></i> <span><?php echo $this->lang->line('nav_requestpage'); ?></span></a>
        </li>
        <li data-href='ticket' class='ticket' id='ticketpage' onclick="pageTicket()" style="display:none">
          <a><i class='fas fa-wrench'></i> <span><?php echo $this->lang->line('nav_ticketpage'); ?></span></a>
        </li>
        <li data-href='report' class='report' id='reportpage' onclick="pageReport()" style="display:none">
          <a><i class="fas fa-address-book"></i><span><?php echo $this->lang->line('nav_reportpage'); ?></span></a>
        </li>
        <li data-href='administrator' class='administrator' id='administratorpage' onclick="pageAdministrator()" style="display:none">
          <a><i class='fas fa-users-cog'></i><span><?php echo $this->lang->line('nav_administratorpage'); ?></span></a>
        </li>
        <li data-href='system' class='system' id='systempage' onclick="pageSystem()" style="display:none">
          <a><i class='fas fa-crown'></i><span><?php echo $this->lang->line('nav_systempage'); ?></span></a>
        </li>
        <li data-href='reportsystem' class='reportsystem' id='reportsystem' onclick="pagereportsystem()" style="display:none">
          <a><i class="fas fa-chalkboard-teacher"></i><span><?php echo $this->lang->line('nav_reportsystempage'); ?></span></a>
        </li>
      </div>
      <!-- <div class="group_sidebar">
      </div> -->
      <li class='auth-logout'><a><i class='fas fa-sign-out-alt'></i> <span><?php echo $this->lang->line('signout'); ?></span></a></li>
    </ul>

    <div class="document-container">
        <nav class='navbar-top'>
            <ul class="left">
              <li class='lg-sidbar'>
                <a class='waves-effect brand-bars opend'>
                  <span class='fas fa-bars'></span>
                </a>
              </li>
              <li class='md-sidebar'>
                <a class='waves-effect brand-bars'>
                  <span class='fas fa-bars'></span>
                </a>
              </li>
              <li class='sm-sidebar'  data-activates="slide-out">
                <a class='waves-effect'>
                  <span class='fas fa-bars'></span>
                </a>
              </li>
            </ul>
            <ul class="right">
              <!-- <li >
                <a id="help" class='nav-top-dropdown'>
                  <span class='fas fa-question-circle'></span>
                </a>
              </li> -->
              <!-- <li >
                <a id="invite_notify" class='nav-top-dropdown' data-activates='invite_dropdown'>
                  <span class='fas fa-envelope'></span>
                  <span class='fas fa-circle notify animated infinite tada'></span>
                </a>
              </li> -->
              <!-- <li>
                <a  id="notify_alert" class='nav-top-dropdown notify-bell'  data-activates='notify_dropdown'>
                  <span class='fas fa-bell'></span>
                  <span class='fas fa-circle notify animated infinite tada'></span>
                </a>
              </li> -->

              <li class='nav-top-dropdown lg-profile' data-activates='profile_dropdown'>
                <a class='bar-lg-profile'>
                  <span class='name'></span>
                  <img  class="auth_profile" >
                  <span class='fas fa-angle-down'></span>
                </a>
              </li>
              <ul id="notify_dropdown"  class='dropdown-content notibody notify-content'></ul>
              <!-- <ul id="invite_dropdown"  class='dropdown-content invitebody invite-content'> -->

              </ul>
              <ul id='profile_dropdown' class='dropdown-content'>
                <li class='get-profile' onclick="pageProfile($(this),'')"><a><i class='fas fa-user-circle'></i> <?php echo $this->lang->line('account'); ?> </a></li>
                <!-- <li><a ><i class='fa fa-cog'></i> <?php echo $this->lang->line('navtop_setting'); ?> </a></li> -->
                <li class="divider"></li>
                <li class='auth-logout'><a><i class='fas fa-sign-out-alt'></i> <?php echo $this->lang->line('signout'); ?> </a></li>
              </ul>
            </ul>
          </nav>
        <div class="sidebar">
              <div class="sidebar-header">
                  <a href="<?php echo base_url("home"); ?>" class='white-text'>
                    <img class='sidebar-logo' src="<?php echo base_url("assets/images/logo/logo.png"); ?>" >
                    <img class='sidebar-logo-text' src="<?php echo base_url("assets/images/logo/logo_text.png");?>" width="80" >
                  </a>
              </div>
              <div class="sidebar-content">
                <ul class="collection">
                  <div class='normal_sidebar'>
                    <li data-href='home' class='home' id='homepage' onclick="pageHome()" >
                      <i class='fas fa-home'></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_requestmepage'); ?></span>
                    </li>
                    <li class="collection-item" class='request' id='requestpage' onclick="pageRequest()">
                      <i class='fas fa-file-alt'></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_requestpage'); ?></span>
                    </li>
                    <li data-href='work' class='work' id='workpage' onclick="pageWorking()" >
                      <i class='fas fa-briefcase'><span class='fas fa-circle notify animated infinite tada' style="display:none"></span></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_workmepage'); ?></span>
                    </li>
                    <li data-href='ticket' class='ticket' id='ticketpage' onclick="pageTicket()" style="display:none">
                      <i class='fas fa-wrench'><span class='fas fa-circle notify animated infinite tada' style="display:none"></span></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_ticketpage'); ?></span>
                    </li>
                    <li data-href='report' class='report' id='reportpage' onclick="pageReport()" style="display:none">
                      <i class="fas fa-address-book"></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_reportpage'); ?></span>
                    </li>
                    <li data-href='administrator' class='administrator' id='administratorpage' onclick="pageAdministrator()" style="display:none">
                      <i class='fas fa-users-cog'></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_administratorpage'); ?></span>
                    </li>
                    <li data-href='system' class='system' id='systempage' onclick="pageSystem()" style="display:none">
                      <i class='fas fa-crown'></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_systempage'); ?></span>
                    </li>
                    <li data-href='reportsystem' class='reportsystem' id='reportsystempage' onclick="pagereportsystem()" style="display:none">
                      <i class='fas fa-chalkboard-teacher'></i>
                      <span class='side-menu-text'><?php echo $this->lang->line('nav_reportpage'); ?></span>
                    </li>
                  </div>
                  <!-- <div class="group_sidebar">
                  </div> -->
                </ul>
              </div>
          </div>
        <div class="main">
            <?php if ($middle) echo $middle; ?>
        </div>
      </div>

      <div class="loader">
        <div class="loader-inner">
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
        </div>
        <div class="white-text loader-inner-text"><?php echo $this->lang->line('load'); ?><span class="white-text point-wait"></span></div>
     </div>

    <div class="modal-info"></div>
    <div class="modal-area"></div>
    <div class="modal-bottom"></div>
    <div class="modal-backdrop"></div>
    <div class="preload-backdrop"></div>
      <code>
            <script id="jsonLang" type="application/json">
                  <?php include("assets/json/lang/thailand.json"); ?>
            </script>
      </code>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/apps/jHelper.js"); ?>"></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/js/apps/jLayouts.js"); ?>'></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/apps/JAuthentication.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/bower_components/counterup/jquery.counterup.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/bower_components/waypoints/lib/jquery.waypoints.min.js"); ?>"></script>
    <script type="text/javascript" src='<?php echo base_url('assets/plugin/js/pretty.js'); ?>'></script>
    <script type="text/javascript" src='<?php echo base_url('assets/plugin/js/md5/md5.js'); ?>'></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/chart.js/dist/Chart.js"); ?>'></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/chart.js/dist/Chart.min.js"); ?>'></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/datepicker/dist/datepicker.js"); ?>' ></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/timepicker/js/timepicki.js"); ?>' ></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/js/PDFObject/pdfobject.js"); ?>' ></script>

    <!-- <script src="https://js.pusher.com/4.1/pusher.min.js"></script> -->

    <?php if (isset($js)){echo $js;} ?>


    </body>
</html>
