<!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/materialize/css/materialize.min.css"); ?>"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/animate.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/login_layout.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/global.css"); ?>"/>
      <link rel="icon" href="<?php echo  base_url("assets/images/logo/logo.png")?>" type="image/gif" sizes="16x16">
      <title> Bullet  - Login </title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <script type="text/javascript" src="<?php echo base_url("assets/plugin/fontawesome/svg-with-js/js/fontawesome-all.min.js"); ?>"></script>
    </head>
    <body>
      <div class="loader">
      	<div class="loader-inner">
      		<div class="loader-line-wrap">
      			<div class="loader-line"></div>
      		</div>
      		<div class="loader-line-wrap">
      			<div class="loader-line"></div>
      		</div>
      		<div class="loader-line-wrap">
      			<div class="loader-line"></div>
      		</div>
      		<div class="loader-line-wrap">
      			<div class="loader-line"></div>
      		</div>
      		<div class="loader-line-wrap">
      			<div class="loader-line"></div>
      		</div>
      	</div>
      </div>
      <div class="modal-info"></div>
      <div class="modal-area"></div>
      <div class="modal-bottom"></div>
      <div class="backdrop"></div>
      <div class="preload-backdrop"></div>
        <?php if ($middle) echo $middle; ?>
      <div class="by-footer">
                 Design by | <a class="red-text text-darken-1">Siamrajathanee Co., Ltd</a>
      </div>
      <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/jquery-3.2.1.min.js"); ?>"></script>
      <script type="text/javascript" src="<?php echo base_url("assets/plugin/materialize/js/materialize.min.js"); ?>"></script>
      <code>
        <script id="jsonLang" type="application/json">
              <?php include("assets/json/lang/thailand.json"); ?>
        </script>
      </code>
      <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/apps/jHelper.js");?>"></script>
      <script type='text/javascript' src='<?php echo base_url("assets/plugin/js/apps/jLayouts.js"); ?>'></script>
      <script type='text/javascript' src='<?php echo base_url("assets/plugin/js/md5/md5.min.js"); ?>'></script>
      <?php if (isset($js)){echo $js;} ?>
  </body>
</html>
