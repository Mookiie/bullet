<div class="main-container">
  <h4 class="box-title m-b-20"><?php echo $this->lang->line('administrator') ?></h4>
  <div class="row">
    <!-- create -->
    <div class="col s6 m4 l3 company-card" style="display:none" onclick="pageCreateCompany()">
      <div class="card" onclick="">
        <div class="card-ticket  center-align">
          <div class="col s12">
            <h3 class='icon'><span class='fas fa-building'></span> </h3>
            <p><span><?php echo $this->lang->line('createcompany');?></span> </p>
          </div>
        </div>
      </div>
    </div>
    <!-- create -->
    <!-- create -->
    <div class="col s6 m4 l3">
      <div class="card" onclick="pageCreateUser()">
        <div class="card-ticket  center-align">
          <div class="col s12">
            <h3 class='icon'><span class='fas fa-user-tie'></span> </h3>
            <p><span><?php echo $this->lang->line('createuser');?></span> </p>
          </div>
        </div>
      </div>
    </div>
    <!-- create -->
  </div>
</div>
