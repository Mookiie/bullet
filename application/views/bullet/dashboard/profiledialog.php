<div id="ProfileDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span><?php echo $profile["fname"]." ".$profile["lname"] ?></span>
    </div>
    <div>
      <div class="row">
        <ul class="tabs tabs-member tabs-fixed-width">
          <li class="tab waves-effect"><a class="details" href="#user_profile"><span class='fas fa-user-circle'></span> <span class='hide-on-small-only'></span></a></li>
          <li class="tab waves-effect"><a class="activity" href="#user_activity"><span class='fas fa-edit'></span> <span class='hide-on-small-only'> </span></a></li>
          <!-- <li class="tab waves-effect"><a class="track" href="#ticket_tracking"><span class='fas fa-tasks'></span> <span class='hide-on-small-only'> </span></a></li> -->
          <!-- <li class="tab waves-effect"><a class="edit_ticket" href="#ticket_edit"><span class='fas fa-edit'></span> <span class='hide-on-small-only'> </span></a></li> -->
        </ul>
      </div>
      <div class="col s12">
        <div id="user_profile">
          user_profile
        </div>
        <div id="user_activity">
          user_activity
        </div>
        <!-- <div id="ticket_tracking">
          ticket_tracking
        </div> -->
        <!-- <div id="ticket_edit">
          ticket_edit
        </div> -->
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat closemodal">OK</a>
    </div>
  </div>
</div>
