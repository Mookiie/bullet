<div class="main-container">
  <h4 class="box-title m-b-20"><?php echo $this->lang->line('dashboard') ?></h4>
  <div class="row">
    <!-- request -->
      <!-- <div class="col s12 m6 l4">
        <div class="card card-count">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-file'></span> </h3>
                       <p><span><?php echo $this->lang->line('request');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-request" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div> -->
    <!-- end request -->
    <!-- wait -->
      <!-- <div class="col s12 m6 l4">
        <div class="card card-count">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-clock'></span> </h3>
                       <p><span><?php echo $this->lang->line('wait');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-wait" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div> -->
    <!-- end wait -->
    <!-- working -->
      <!-- <div class="col s12 m6 l4">
        <div class="card card-count">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-walking'></span> </h3>
                       <p><span><?php echo $this->lang->line('working');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-working" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div> -->
    <!-- end working -->
    <!-- check -->
      <!-- <div class="col s12 m6 l4">
        <div class="card card-count">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-calendar-check'></span> </h3>
                       <p><span><?php echo $this->lang->line('check');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-check" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div> -->
    <!-- end check -->
    <!-- success -->
      <!-- <div class="col s12 m6 l4">
        <div class="card card-count">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-trophy'></span> </h3>
                       <p><span><?php echo $this->lang->line('success');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-success" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div> -->
    <!-- end success -->
    <!-- add -->
      <!-- <div class="col s12 m6 l4 " onclick="pageRequest()">
        <div class="card card-count">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-plus'></span> </h3>
                       <p><span><?php //echo $this->lang->line('add');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h5 class="count" style="margin-top: 1.25em;"><?php echo $this->lang->line('add');?></h5>
                     </div>
                </div>
            </div>
        </div>
      </div> -->
    <!-- end add -->

    <!-- request -->
      <div class="col s6 m3 l3">
        <div class="card card-count" onclick="ticket_wait()">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-file'></span> </h3>
                       <p><span><?php echo $this->lang->line('request');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count counter" id="count-request" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div>
    <!-- end request -->
    <!-- working -->
      <div class="col s6 m3 l3">
        <div class="card card-count" onclick="ticket_working()">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-walking'></span> </h3>
                       <p><span><?php echo $this->lang->line('status_working');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-working" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div>
    <!-- end working -->
    <!-- success -->
      <div class="col s6 m3 l3">
        <div class="card card-count" onclick="ticket_success()">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-trophy'></span> </h3>
                       <p><span><?php echo $this->lang->line('success');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h2 class="count" id="count-success" style="margin: 0;"></h2>
                     </div>
                </div>
            </div>
        </div>
      </div>
    <!-- end success -->
    <!-- add -->
      <div class="col s6 m3 l3" onclick="pageRequest()">
        <div class="card card-count ">
            <div class="card-content  center-align">
                <div class="row">
                     <div class="col s12">
                       <h3 class='icon'><span class='fas fa-plus'></span> </h3>
                       <p><span><?php //echo $this->lang->line('add');?></span> </p>
                     </div>
                     <div class="col s12">
                       <h5 class="count" style="margin-top: 1.25em;"><?php echo $this->lang->line('add');?></h5>
                     </div>
                </div>
            </div>
        </div>
      </div>
    <!-- end add -->
    <div class="row">
      <button class="btn btn-all" type="button" name="button" style="width:100%" onclick="ticketme('')"><?php echo $this->lang->line('status_all')." " ?><span id="count-record" style="color:#FFFFFF;"></span> <?php echo " ".$this->lang->line('list') ?></button>
    </div>

    <!-- <div class="list">
      <table>
        <tr style="background:#CCC">
          <td class="center"><span><?php echo " ".$this->lang->line('list') ?></span></td>
        </tr>
      </table>
    </div> -->
    <!-- table -->
    <div class="row file-toolbar">
      <div class="col m4 l3 right-align hide-on-small-only">
        <table class='select-row'>
          <tr>
            <td>
              <label class='label-for-sm' for="member_row_show"><?php echo $this->lang->line('table_label_show'); ?> </label>
            </td>
            <td>
              <select class='browser-default' id='row_show' onchange="getviewTables($(this));">
                <option value="5" selected>5</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
              </select>
            </td>
            <td class='right-align '>
              <label class='label-for-sm' for="member_row_show"><?php echo $this->lang->line('table_label_row'); ?> </label>
            </td>
          </tr>
        </table>
      </div>

      <div class="col m5 l4 row">
          <input class="search-text" placeholder="<?php echo $this->lang->line('table_search'); ?> "  type="text" id="searchTable" onkeyup="searchInTables($(this));">
      </div>

      <div class="col s5 m3 l5 right-align">
        <div class="table-pagination btn-group">
          <button type="button" name="button" class='btn btn-primary prev-page' onclick="prevpage(this);">
            <span class='fa fa-angle-left'></span>
          </button>
          <button type="button" name="button" class='btn btn-default page-num ' >
          <span id='nowpage'>1 </span><span> / </span><span class='totalpage'></span>
          </button>
          <button type="button" name="button" class='btn btn-primary next-page' onclick="nextpage(this);">
            <span class='fa fa-angle-right'></span>
          </button>
        </div>
      </div>
    </div>
    <div class="">
      <table id="tbService">
            <thead>
              <tr>
                  <th width="10%"><?php echo $this->lang->line('ticket_status') ?></th>
                  <th width="40%"><?php echo $this->lang->line('ticket_title') ?></th>
                  <th width="25%"><?php echo $this->lang->line('ticket_date') ?></th>
                  <th width="35%"><?php echo $this->lang->line('ticket_progress') ?>
                    <table>
                      <tr>
                        <td width="20%" style="background:#C10230; margin:0px !important; padding:0px; text-align:center; border-radius:0;"><small class="black-text" >wait</small></td>
                        <td width="20%" style="background:#FF5100; margin:0px !important; padding:0px; text-align:center; border-radius:0;"><small class="black-text" >assign</small></td>
                        <td width="20%" style="background:#FFD600; margin:0px !important; padding:0px; text-align:center; border-radius:0;"><small class="black-text" >working</small></td>
                        <td width="20%" style="background:#00C08B; margin:0px !important; padding:0px; text-align:center; border-radius:0;"><small class="black-text" >checking</small></td>
                        <td width="20%" style="background:#00C4B3; margin:0px !important; padding:0px; text-align:center; border-radius:0;"><small class="black-text" >success</small></td>
                      </tr>
                    </table>
                  </th>
              </tr>
            </thead>
            <tbody class="file-container">
              <!-- file-container -->
            </tbody>
      </table>
    </div>
    <!-- end table -->

  </div>
</div>
