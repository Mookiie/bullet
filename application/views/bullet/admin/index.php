<div class="main-container">
  <h4 class="box-title m-b-20"><?php echo $this->lang->line('administrator') ?></h4>
  <div class="row">
    <!-- invite -->
    <div class="col s6 m4 l3">
      <div class="card" onclick="pageInvite()">
        <div class="card-ticket  center-align">
          <div class="col s12">
            <h3 class='icon'><span class='fas fa-user'></span> </h3>
            <p><span><?php echo $this->lang->line('invite');?></span> </p>
          </div>
        </div>
      </div>
    </div>
    <!-- invite -->
    <!-- view -->
    <div class="col s6 m4 l3">
      <div class="card" onclick="pageViewUser()">
        <div class="card-ticket  center-align">
          <div class="col s12">
            <h3 class='icon'><span class='fas fa-eye'></span> </h3>
            <p><span><?php echo $this->lang->line('view');?></span> </p>
          </div>
        </div>
      </div>
    </div>
    <!-- view -->
  </div>
</div>
