<div class="main-container">
  <h4 class="box-title m-b-20"><?php echo $this->lang->line('request') ?></h4>
  <form class="" id="formTicket" method="post">
    <div class="card-container">

      <div class="form-group company-select" style="display:none">
        <label><?php echo $this->lang->line('request_company') ?></label>
        <select class="browser-default company" id="company" name="company">
          <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('request_company') ?></option>
        </select>
      </div>

      <div class="form-group ">
        <label><?php echo $this->lang->line('request_branch') ?></label>
        <select class="browser-default branch" id="branch" name="branch" required="">
          <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('request_branch') ?></option>
        </select>
      </div>

      <div class="form-group ">
        <label><?php echo $this->lang->line('request_dep') ?></label>
        <select class="browser-default department" id="dep" name="dep" required="" disabled="">
          <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('request_dep') ?></option>
        </select>
      </div>

      <div class="form-group ">
        <label><?php echo $this->lang->line('request_site') ?></label>
        <select class="browser-default site" id="site" name="site" disabled="">
          <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('request_site') ?></option>
        </select>
      </div>

      <div class="form-group ">
        <label><?php echo $this->lang->line('request_file') ?></label>
        <div class="file-field input-field">
          <div class="btn">
            <span style="color:#ffffff;">File</span>
            <input type="file" id="file" name="file">
          </div>
          <div class="file-path-wrapper">
            <input class="form-control file-path" type="text" id="nfile" name="nfile">
          </div>
        </div>
      </div>

      <div class="form-group ">
        <label><?php echo $this->lang->line('request_title') ?></label>
        <div class="col s12" id="username_group">
          <input class="form-control" type="text" name="title" id='title' keypress="return onremove_validate(this);" required="" placeholder="<?php echo $this->lang->line('request_title') ?>">
          <div class='feedback' id="title_feedback"></div>
        </div>
      </div>

      <div class="form-group ">
        <label><?php echo $this->lang->line('request_detail') ?></label>
        <div class="col s12" id="username_group">
          <textarea class="form-control materialize-textarea" id='detail' name="detail" placeholder="<?php echo $this->lang->line('request_detail') ?>" keypress="return onremove_validate(this);" required=""></textarea>
          <div class='feedback' id="detail_feedback"></div>
        </div>
      </div>

      <div class="form-group right">
        <div class="row">
          <button class="btn waves-effect waves-light btn-request " type="submit" name="button"> send</button>
          <button class="btn waves-effect waves-light " type="reset" name="button"> cancel</button>
        </div>
      </div>

    </div>
  </form>
</div>
