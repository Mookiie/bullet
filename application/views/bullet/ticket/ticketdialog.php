<div id="TicketDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span><?php echo $ticket["ticket_title"]." (".$ticket["ticket_id"].")" ?></span>
      <span class="right fa fa-times closemodal" style="
padding: 5px 15px;"></span>
    </div>

      <div class="row">
        <ul class="tabs tabs-member tabs-fixed-width">
          <li class="tab waves-effect details"><a href="#ticket_profile"><span class='fas fa-user-circle'></span> <span class='hide-on-small-only'></span></a></li>
          <li class="tab waves-effect attached" style="display:none"><a href="#ticket_attached"><span class='fas fa-paperclip'></span> <span class='hide-on-small-only'> </span></a></li>
          <li class="tab waves-effect track"><a id="tracking" href="#ticket_tracking"><span class='fas fa-tasks'></span> <span class='hide-on-small-only'> </span></a></li>
          <li class="tab waves-effect detail" style="display:none"><a href="#ticket_detail"><span class='fas fa-info'></span> <span class='hide-on-small-only'> </span></a></li>
          <!-- <li class="tab waves-effect assign" style="display:none"><a  href="#ticket_assign"><span class='fas fa-cogs'></span> <span class='hide-on-small-only'> </span></a></li> -->
          <li class="tab waves-effect assign-config" style="display:none"><a id="assign_config" href="#ticket_seting"><span class='fas fa-cogs'></span> <span class='hide-on-small-only'> </span></a></li>
          <li class="tab waves-effect edit" style="display:none"><a id="edit_config" class="edit_ticket" href="#ticket_edit"><span class='fas fa-edit'></span> <span class='hide-on-small-only'> </span></a></li>
        </ul>
      </div>

      <div class="col s12">

        <div id="ticket_profile">
          <div class="row">
            <div class="row card-container">
              <div class="right user-menu" style="display:none">
                <span>คุณต้องการ </span>
                <button class="btn waves-effect waves-light from btn-warning" type="button" id="edit" name="button" > <?php echo $this->lang->line('edit') ?></button>
                <button class="btn waves-effect waves-light from btn-danger " type="button" id="dismiss-u" name="button" > <?php echo $this->lang->line('dismiss') ?></button>
              </div>
              <div class="right work-menu" style="display:none">
                <span>คุณต้องการ </span>
                <button class="btn waves-effect waves-light from btn-success" type="button" id="working" name="button" > <?php echo $this->lang->line('accept') ?></button>
                <button class="btn waves-effect waves-light from btn-danger " type="button" id="dismiss-w" name="button" > <?php echo $this->lang->line('dismiss') ?></button>
              </div>
              <div class="right assign-menu" style="display:none">
                <span>คุณต้องการ </span>
                <button class="btn waves-effect waves-light from btn-success" type="button" id="assign" name="button" > <?php echo $this->lang->line('assign') ?></button>
                <button class="btn waves-effect waves-light from btn-danger " type="button" id="dismiss-t" name="button" > <?php echo $this->lang->line('dismiss') ?></button>
              </div>
            </div>
            <div class="card-container">
              <div class="col s12">
                <div class="card">
                  <div class="card-action">
                    <u>ข้อมูลคำขอ <?php echo $ticket["ticket_id"] ?></u>
                  </div>
                  <div class="card-content ">
                    <dt>
                      ชื่อ : <?php echo $ticket["ticket_title"] ?> <br>
                      สาขา : <?php echo ($ticket['branch_name'] == '') ? "ไม่ระบุ" : $ticket['branch_name']; ?> <br>
                      แผนก : <?php echo ($ticket['dep_name'] == '') ? "ไม่ระบุ" : $ticket['dep_name']; ?> <br>
                      ไซต์งาน : <?php echo ($ticket['site_name'] == '') ? "ไม่ระบุ" : $ticket['site_name']."(".$ticket['site_description'].")"; ?> <br>
                      รายละเอียด : <br>
                    </dt>
                    <dd>
                        <?php echo nl2br($ticket['ticket_detail']) ?>
                    </dd>
                    <dt>
                      ขอเมื่อ : <?php echo date("d M Y H:m", strtotime($ticket["ticket_date"])); ?><br>
                    </dt>
                    <?php if ($ticket["ticket_update"]!= null): ?>
                      <dt>
                        แก้ไขล่าสุดเมื่อ : <?php echo date("d M Y H:m", strtotime($ticket["ticket_udate"])); ?><br>
                      </dt>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col s12">
                <div class="card">
                  <div class="card-action">
                    <u>ข้อมูลผู้ขอ</u>
                  </div>
                  <div class="card-content ">
                    <dt>
                      ชื่อ : <?php echo $ticket["fname"]." ".$ticket["lname"] ?> <br>
                      สาขา : <?php echo ($ticket['user_branch_name'] == '') ? "ไม่ระบุ" : $ticket['user_branch_name']; ?> <br>
                      แผนก : <?php echo ($ticket['user_dep_name'] == '') ? "ไม่ระบุ" : $ticket['user_dep_name']; ?> <br>
                      ไซต์งาน : <?php echo ($ticket['user_site_name'] == '') ? "ไม่ระบุ" : $ticket['user_site_name']; ?> <br>
                      ทีม : <?php echo ($ticket['user_team_name'] == '') ? "ไม่ระบุ" : $ticket['user_team_name']; ?>
                    </dt>
                  </div>
                </div>
              </div>
              <div class="col s12 dialogservice" style="display:none">
                <div class="card">
                  <div class="card-action">
                    <u>ข้อมูลการให้บริการ</u>
                  </div>
                  <div class="card-content ">
                    <dt>
                      ชื่อเจ้าหน้าที่ : <?php echo $ticket["assign_to"]; ?><br>
                      เริ่มงาน :  <?php echo date("d M Y", strtotime($ticket["work_start"])); ?><br>
                      วันครบกำหนด :  <?php echo date("d M Y", strtotime($ticket["work_finish"])); ?><br>
                      มอบหมายโดยโดย :  <?php echo $ticket["assign_by"]; ?><br>
                      ตรวจสอบโดย : <?php echo ($ticket['qc_by'] == '') ? "ยังดำเนินการไม่เสร็จ" : $ticket['qc_by']." (".$ticket['qc_at'].")"; ?><br>
                      <!-- <a class="right">เพิ่มเติม >></a> -->
                      <br>
                    </dt>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="ticket_attached">
          <?php
          $doc = $ticket["ticket_image"];
          if($doc == ""){
              echo "<div class='card-block' align='center'><h2><span class='	fa fa-exclamation-triangle'></span> Not Found Document</h2></div>";
          }else{
            $file = explode(".",$doc);
            $type = $file[count($file)-1];
            if($type == "pdf"){
              echo "<input type='hidden' id='pathtxt' value='".$doc."'>";
              echo "<div id='framPDF' style='height:800px;'></div>";
            }else{ ?>
              <div class="row right card-container">
                <input type='hidden' id='pathtxt' value='' >
                  <a class=" btn nav-link" id="download" href="<?php echo $doc;?>" download="<?php echo $ticket["ticket_id"];?>">
                    <span class='fa fa-download'></span>
                  </a>
                  <!-- <a class=" btn nav-link"  onclick="VoucherPrint('<?php echo $doc;?>')">
                    <span class='fa fa-print'></span>
                  </a> -->
              </div>
              <img width='100%' class='bkImg' src='<?php echo $doc;?>'>

          <?php  }
          }
          ?>
        </div>
        <div id="ticket_tracking">
          <div class="card-container">
            <div class="col s12">
              <div class="card-content ">
                <table>
                  <tr style="border-bottom:solid 1px #ddd;">
                    <td>
                      <label for="detail"><span class='fas fa-list'></span> <?php echo $this->lang->line('tracking'); ?> </label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div id='tracking_list'></div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div id="ticket_detail">
          <div class="row card-container">
            <div class="right qc-menu" style="display:none">
              <span>ตรวจสอบ </span>
              <button class="btn waves-effect waves-light from btn-success " type="button" id="pass" name="button"> <?php echo $this->lang->line('pass') ?></button>
              <button class="btn waves-effect waves-light from btn-danger  " type="button" id="fail" name="button"> <?php echo $this->lang->line('fail') ?></button>
            </div>
            <div class="right detail-menu" style="display:none">
              <span>คุณต้องการ </span>
              <button class="btn waves-effect waves-light from btn-warning" type="button" id="detail" name="button" > <?php echo $this->lang->line('detail') ?></button>
              <button class="btn waves-effect waves-light from btn-success " type="button" id="success" name="button" disabled> <?php echo $this->lang->line('success') ?></button>
            </div>
          </div>
          <div class="card-container">
            <div class="add-detail" style="display:none">
              <from id="formDetailTicket" method="post">
                <div class="card-container">
                  <input type="hidden" name="" id="work_id" value="<?php echo $ticket["work_id"];?>">
                  <div class="form-group ">
                    <label><?php echo $this->lang->line('working_file') ?></label>
                    <div class="file-field input-field">
                      <div class="btn">
                        <span style="color:#ffffff;">File</span>
                        <input type="file" id="file" name="file">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="form-control file-path" type="text" id="nfile" name="nfile">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('working_detail') ?></label>
                    <div class="col s12" id="workingdetail_group">
                      <textarea class="form-control materialize-textarea" id='workingdetail' name="detail" placeholder="<?php echo $this->lang->line('working_detail') ?>" keypress="return onremove_validate(this);" required=""></textarea>
                      <div class='feedback' id="workingdetail_feedback"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <button class="btn waves-effect waves-light btn-request " type="submit" name="button" id="send-add"> send</button>
                      <button class="btn waves-effect waves-light" type="reset" name="button" id="close-add"> cancel</button>
                    </div>
                  </div>
                </div>
              </from>
            </div>
            <div class="show-detail">
            </div>
          </div>
        </div>
        <div id="ticket_seting">
          <form id="formAssign" class="" method="post">
            <div class="card-container">
              <input type="hidden" id="work_id" value="">
              <div class="form-group">
                <label><?php echo $this->lang->line('assign_type') ?></label>
                <select class="browser-default" id="type" name="type" required="" >
                  <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('assign_type') ?></option>
                  <option value="1" class="select-user"><?php echo $this->lang->line('type_user')?></option>
                  <option value="2" class="select-team"><?php echo $this->lang->line('type_team')?></option>
                </select>
              </div>

              <div class="form-group assign-team" style="display:none">
                <label><?php echo $this->lang->line('assign_team') ?></label>
                <select class="browser-default assign_team" id="assign_team" name="assign_team" disabled="">
                  <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('assign_team') ?></option>
                </select>
              </div>

              <div class="form-group assign-user" style="display:none">
                <label><?php echo $this->lang->line('assign_user') ?></label>
                <select class="browser-default assign_user" id="assign_user" name="assign_user" required="" disabled="">
                  <option value="" disabled selected><?php echo $this->lang->line('select_please').$this->lang->line('assign_user') ?></option>
                </select>
              </div>

              <div class="form-group">
                <label><?php echo $this->lang->line('assign_finish') ?></label>
                <input type="text" class="datepicker" id="assign_finish" name="assign_finish" placeholder="<?php echo $this->lang->line('select_please').$this->lang->line('assign_finish') ?>" required="">
              </div>

              <div class="form-group">
                <div class="row center">
                  <button class="btn waves-effect waves-light " type="submit" name="button"> send </button>
                  <button class="btn waves-effect waves-light " type="reset" name="button"> cancel </button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div id="ticket_edit">
          <form class="" id="formEditTicket" method="post">
            <div class="card-container">
               <div class="form-group ">
                 <label><?php echo $this->lang->line('request_file') ?></label>
                 <div class="file-field input-field">
                   <div class="btn">
                     <span style="color:#ffffff;">File</span>
                     <input type="file" id="file" name="file">
                   </div>
                   <div class="file-path-wrapper">
                     <input class="form-control file-path" type="text" id="nfile" name="nfile">
                   </div>
                 </div>
               </div>

               <div class="form-group ">
                 <label><?php echo $this->lang->line('request_title') ?></label>
                 <div class="col s12" id="username_group">
                   <input class="form-control" type="text" name="title" id='title-edit' keypress="return onremove_validate(this);" required="" placeholder="<?php echo $this->lang->line('request_title') ?>" value="<?php echo $ticket["ticket_title"] ?>">
                   <div class='feedback' id="title_feedback"></div>
                 </div>
               </div>

               <div class="form-group ">
                 <label><?php echo $this->lang->line('request_detail') ?></label>
                 <div class="col s12" id="detail_group">
                   <textarea class="form-control materialize-textarea" id='detail-edit' name="detail" placeholder="<?php echo $this->lang->line('request_detail') ?>" keypress="return onremove_validate(this);" required=""><?php echo nl2br($ticket['ticket_detail'])?></textarea>
                   <div class='feedback' id="detail_feedback"></div>
                 </div>
               </div>

               <div class="form-group">
                 <div class="row">
                   <button class="btn waves-effect waves-light btn-edit " type="submit" name="button"> edit</button>
                   <button class="btn waves-effect waves-light " type="reset" name="button"> cancel</button>
                 </div>
               </div>

            </div>
          </form>
        </div>

      </div>
    <div class="modal-footer">
      <div class="right">
        <button class="btn waves-effect waves-light closemodal" type="button" name="button"> <?php echo $this->lang->line('close') ?></button>
      </div>
    </div>
  </div>
</div>
